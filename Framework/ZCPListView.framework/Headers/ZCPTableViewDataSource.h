//
//  ZCPTableViewDataSource.h
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCell.h"
#import "ZCPTableViewCellViewModel.h"
#import "ZCPTableViewSectionView.h"
#import "ZCPTableViewSectionDataModel.h"
@protocol ZCPTableViewDataSourceListenerProtocol;
@class ZCPTableViewDataSource;

NS_ASSUME_NONNULL_BEGIN

/// UITableView 数据源协议
@protocol ZCPTableViewDataSourceProtocol <UITableViewDataSource>

/// section 数据
@property (nonatomic, strong) NSMutableArray <ZCPTableViewSectionDataModel *>*sectionDataModelArray;
/// 监听对象
@property (nonatomic, weak, nullable) id<ZCPTableViewDataSourceListenerProtocol> listener;

// 工具方法
/// 返回指定索引位置的 section 数据模型
- (nullable ZCPTableViewSectionDataModel *)dataModelForSectionInSection:(NSInteger)section;
/// 返回指定索引位置的 cell 视图模型
- (nullable ZCPTableViewCellViewModel *)viewModelForRowAtIndexPath:(NSIndexPath *)indexPath;
/// 返回 section 个数
- (NSInteger)numberOfSections;
/// 返回 指定 section 下 cell 的个数
/// @param section 指定section
- (NSInteger)numberOfRowsInSection:(NSInteger)section;
/// 返回 section 索引列表
- (NSArray *)sectionIndexTitles;

// 用于 UITableViewDelegate 的补充方法
/// 返回指定索引位置的 cell 高度
- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath;
/// 返回指定索引位置的 section header 高度
- (CGFloat)heightForHeaderInSection:(NSInteger)section;
/// 返回指定索引位置的 section header 高度
- (CGFloat)heightForFooterInSection:(NSInteger)section;
/// 返回指定索引位置的 section header view
- (nullable UIView *)viewForHeaderInSection:(NSInteger)section;
/// 返回指定索引位置的 section footer view
- (nullable UIView *)viewForFooterInSection:(NSInteger)section;
/// 返回指定索引位置的 cell 在编辑状态下的样式
- (UITableViewCellEditingStyle)editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath;
/// 返回指定索引位置的 cell 的侧滑编辑按钮数组
- (nullable NSArray<UITableViewRowAction *> *)editActionsForRowAtIndexPath:(NSIndexPath *)indexPath;
/// 返回指定索引位置的 cell 的侧滑删除按钮的标题，该按钮是仅有一个系统删除按钮的情况
- (nullable NSString *)titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath;
/// 返回指定索引位置的 cell 在编辑状态下左侧是否缩进
- (BOOL)shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath;

@end

/// UITableView 数据源监听者协议
@protocol ZCPTableViewDataSourceListenerProtocol <NSObject>

@optional

/// 监听 cell 将要更新事件
/// @param dataSource 数据源对象
/// @param cell 当前被更新的 cell
/// @param viewModel 当前用于更新 cell 的 viewModel
/// @param indexPath 索引
- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource willUpdateCell:(ZCPTableViewCell *)cell withViewModel:(ZCPTableViewCellViewModel *)viewModel atIndexPath:(NSIndexPath *)indexPath;

/// 监听 cell 更新完成事件
/// @param dataSource 数据源对象
/// @param cell 当前被更新的 cell
/// @param viewModel 当前用于更新 cell 的 viewModel
/// @param indexPath 索引
- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource didUpdateCell:(ZCPTableViewCell *)cell withViewModel:(ZCPTableViewCellViewModel *)viewModel atIndexPath:(NSIndexPath *)indexPath;

/// 监听 header 将要更新事件
/// @param dataSource 数据源对象
/// @param headerView 当前被更新的 section头视图
/// @param viewModel 当前用于更新的 viewModel
/// @param section 索引
- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource willUpdateHeader:(ZCPTableViewSectionView *)headerView withViewModel:(ZCPTableViewSectionViewModel *)viewModel inSection:(NSInteger)section;

/// 监听 header 更新完成事件
/// @param dataSource 数据源对象
/// @param headerView 当前被更新的 section头视图
/// @param viewModel 当前用于更新的 viewModel
/// @param section 索引
- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource didUpdateHeader:(ZCPTableViewSectionView *)headerView withViewModel:(ZCPTableViewSectionViewModel *)viewModel inSection:(NSInteger)section;

/// 监听 footer 将要更新事件
/// @param dataSource 数据源对象
/// @param footerView 当前被更新的 section脚视图
/// @param viewModel 当前用于更新的 viewModel
/// @param section 索引
- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource willUpdateFooter:(ZCPTableViewSectionView *)footerView withViewModel:(ZCPTableViewSectionViewModel *)viewModel inSection:(NSInteger)section;

/// 监听 footer 更新完成事件
/// @param dataSource 数据源对象
/// @param footerView 当前被更新的 section脚视图
/// @param viewModel 当前用于更新的 viewModel
/// @param section 索引
- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource didUpdateFooter:(ZCPTableViewSectionView *)footerView withViewModel:(ZCPTableViewSectionViewModel *)viewModel inSection:(NSInteger)section;

/// 编辑状态下 Delete/Insert 按钮的点击回调。个人觉得这个方法应该放在 UITableViewDelegate中，比较疑惑为什么在UITableViewDataSource里面。
/// 这个回调会在下面两种情况下被调用：1.编辑状态下的Insert按钮点击事件 2.左滑后的非action的Delete按钮点击事件
/// @param tableView This UITableView
/// @param editingStyle 点击的按钮样式
/// @param indexPath 索引
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath;

/// <#Description#>
/// @param tableView <#tableView description#>
/// @param sourceIndexPath <#sourceIndexPath description#>
/// @param destinationIndexPath <#destinationIndexPath description#>
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath;

@end

/// UITableView 数据源
@interface ZCPTableViewDataSource : NSObject <ZCPTableViewDataSourceProtocol>

@end

/// UITableViewDelegate 工具宏
/// 此处的宏是为了方便 UITableViewDelegate 中需要返回 ZCPTableViewDataSource 中数据的方法实现。
/// 可以满足1个或多个 tableview 的 ZCPTableViewDataSource 接入，若需要其他类实现UITableViewDataSource，则需要自己写下列方法。
#undef UITableViewDelegate_ZCP_BASE_IMP
#define UITableViewDelegate_ZCP_BASE_IMP \
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource heightForRowAtIndexPath:indexPath]; \
    } \
    return 0; \
} \
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource heightForHeaderInSection:section]; \
    } \
    return 0; \
} \
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource viewForHeaderInSection:section]; \
    } \
    return nil; \
} \
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource heightForFooterInSection:section]; \
    } \
    return 0; \
} \
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource viewForFooterInSection:section]; \
    } \
    return nil; \
}


#undef UITableViewDelegate_ZCP_EDIT_IMP
#define UITableViewDelegate_ZCP_EDIT_IMP \
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource editingStyleForRowAtIndexPath:indexPath]; \
    } \
    return UITableViewCellEditingStyleNone; \
} \
- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource editActionsForRowAtIndexPath:indexPath]; \
    } \
    return nil; \
} \
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource titleForDeleteConfirmationButtonForRowAtIndexPath:indexPath]; \
    } \
    return nil; \
} \
- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath { \
    if ([tableView.dataSource isKindOfClass:[ZCPTableViewDataSource class]]) { \
        return [(ZCPTableViewDataSource *)tableView.dataSource shouldIndentWhileEditingRowAtIndexPath:indexPath]; \
    } \
    return YES; \
}

NS_ASSUME_NONNULL_END
