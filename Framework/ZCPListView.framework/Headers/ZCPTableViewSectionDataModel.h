//
//  ZCPTableViewSectionDataModel.h
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCellViewModel.h"
#import "ZCPTableViewSectionViewModel.h"

NS_ASSUME_NONNULL_BEGIN

/// Section 数据模型
@interface ZCPTableViewSectionDataModel : NSObject <NSCopying>

/// Header 视图模型
@property (nonatomic, strong) ZCPTableViewSectionViewModel *headerViewModel;
/// Footer 视图模型
@property (nonatomic, strong) ZCPTableViewSectionViewModel *footerViewModel;
/// Section下的 Cell 视图模型数组
@property (nonatomic, strong) NSMutableArray <ZCPTableViewCellViewModel *> *cellViewModelArray;
/// Section 的索引值，设置后将会展示在 TableView 右侧索引栏，点击后会定位到此 Section
@property (nonatomic, copy) NSString *indexTitle;

@end

NS_ASSUME_NONNULL_END
