//
//  ZCPTableViewController.h
//  ZCPListView
//
//  Created by zcp on 16/9/21.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCPTableViewDataSource.h"
#import "ZCPTableViewSingleSectionDataSource.h"

/// UITableView 视图控制器
@interface ZCPTableViewController : UIViewController <UITableViewDelegate>

/// UITableView
@property (nonatomic, strong, readonly) UITableView *tableView;
/// UITableView 数据源
@property (nonatomic, strong, readonly) ZCPTableViewDataSource *tableViewDataSource;
/// 是否是单section数据源，默认为YES
@property (nonatomic, assign, readonly) BOOL isSingleSectionTableView;

/// 实例化单section VC
+ (instancetype)instanceSingleSectionTableViewController;
/// 实例化多section VC
+ (instancetype)instanceMultiSectionTableViewController;
/// 构造数据
- (void)constructData;

@end
