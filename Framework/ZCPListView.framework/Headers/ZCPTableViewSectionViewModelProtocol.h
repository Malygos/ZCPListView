//
//  ZCPTableViewSectionViewModelProtocol.h
//  ZCPListView
//
//  Created by zcp on 2020/4/2.
//

#import <UIKit/UIkit.h>
@protocol ZCPTableViewSectionViewProtocol;

NS_ASSUME_NONNULL_BEGIN

/// Section 视图模型协议
@protocol ZCPTableViewSectionViewModelProtocol <NSObject>

/// SectionView 的类
@property (nonatomic, strong) Class <ZCPTableViewSectionViewProtocol> sectionViewClass;
/// SectionView 的重用标识
@property (nonatomic, copy) NSString *sectionViewReuseIdentifier;
/// 是否使用nib，默认为NO，用于使用 xib 创建的 Section
@property (nonatomic, assign) BOOL useNib;

/// SectionView 的高度
@property (nonatomic, strong) NSNumber *sectionViewHeight;
/// SectionView 的背景颜色，如果传nil，则颜色默认为白色
@property (nonatomic, strong, nullable) UIColor *sectionViewBgColor;

@end

NS_ASSUME_NONNULL_END
