Pod::Spec.new do |spec|

  spec.name         = "ZCPListView"
  spec.version      = "0.0.1"
  spec.summary      = "It`s a UITableView util framework."
  spec.description  = <<-DESC
                      It`s a UITableView util framework.
                   DESC

  spec.homepage     = "https://gitlab.com/Malygos/ZCPListView_iOS"
  spec.license      = { :type => 'MIT', :file => 'LICENSE' }
  spec.author       = { "朱超鹏" => "z164757979@163.com" }
  spec.source       = { :git => "https://gitlab.com/Malygos/ZCPListView_iOS.git", :tag => "#{s.version}" }
  spec.platform     = :ios, '9.0'
  spec.framework    = 'Foundation', 'UIKit'

  # source_files start
  spec.source_files = "Source/ZCPListView.h"
  spec.subspec 'Core' do |spec_core|
    spec_core.source_files = 'Source/Core/**/*.{h,m}'
  end
  spec.subspec 'Extra' do |spec_extra|
    spec_extra.source_files = 'Source/Extra/**/*.{h,m}'
  end
  # source_files end

end
