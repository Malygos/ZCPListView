//
//  SingleSectionTableViewController.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "SingleSectionTableViewController.h"

@implementation SingleSectionTableViewController

@synthesize infoArray = _infoArray;

- (NSArray *)infoArray {
    if (!_infoArray) {
        _infoArray = @[@{@"title": @"MessageExample", @"class": @"MessageExampleTableViewController"},
                       @{@"title": @"CacheHeightExample", @"class": @"CacheHeightExampleViewController"}];
    }
    return _infoArray;
}

@end
