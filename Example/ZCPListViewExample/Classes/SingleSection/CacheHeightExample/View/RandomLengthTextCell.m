//
//  RandomLengthTextCell.m
//  ZCPListViewExample
//
//  Created by 朱超鹏(平安健康互联网健康研究院) on 2020/6/19.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "RandomLengthTextCell.h"

@implementation RandomLengthTextCell

+ (CGFloat)calculateCacheHeightForViewModel:(RandomLengthTextCellViewModel *)viewModel {
    CGFloat height = [super calculateCacheHeightForViewModel:viewModel];
    NSLog(@"[CacheHeightExample] 高度重新计算了：%lf", height);
    return height;
}

@end

@implementation RandomLengthTextCellViewModel

@end
