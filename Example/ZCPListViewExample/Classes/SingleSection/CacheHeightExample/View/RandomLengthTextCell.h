//
//  RandomLengthTextCell.h
//  ZCPListViewExample
//
//  Created by 朱超鹏(平安健康互联网健康研究院) on 2020/6/19.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZCPListView/ZCPListView.h>

NS_ASSUME_NONNULL_BEGIN

@interface RandomLengthTextCell : ZCPTableViewSingleTitleCell

@end

@interface RandomLengthTextCellViewModel : ZCPTableViewSingleTitleCellViewModel

@end

NS_ASSUME_NONNULL_END
