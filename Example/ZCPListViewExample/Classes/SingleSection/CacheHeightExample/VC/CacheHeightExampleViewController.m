//
//  CacheHeightExampleViewController.m
//  ZCPListViewExample
//
//  Created by 朱超鹏(平安健康互联网健康研究院) on 2020/6/19.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "CacheHeightExampleViewController.h"
#import "RandomLengthTextCell.h"
#import <Masonry.h>

@interface CacheHeightExampleViewController () <UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZCPTableViewSingleSectionDataSource *tableViewDataSource;

@end

@implementation CacheHeightExampleViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self reloadTableViewData];
}

#pragma mark - setup

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - private

- (void)reloadTableViewData {
    NSMutableArray *array = [NSMutableArray array];
    {
        [array addObject:[self modelForTitle:@"老铁双击" tag:0]];
    }
    {
        [array addObject:[self modelForTitle:@"卢本伟" tag:1]];
    }
    {
        [array addObject:[self modelForTitle:@"哈哈" tag:2]];
    }
    for (int i = 0; i < 500; i++) {
        [array addObject:[self modelForTitle:@"凑数" tag:3+i]];
    }
    [self.tableViewDataSource.cellViewModelArray removeAllObjects];
    [self.tableViewDataSource.cellViewModelArray addObjectsFromArray:array];
    [self.tableView reloadData];
}

- (RandomLengthTextCellViewModel *)modelForTitle:(NSString *)title tag:(NSInteger)tag {
    RandomLengthTextCellViewModel *model = [[RandomLengthTextCellViewModel alloc] init];
    model.cellClass = [RandomLengthTextCell class];
    model.titleString = title;
    model.titleFont = [UIFont systemFontOfSize:25];
    model.titleNumberOfLines = 0;
    model.bottomSeparatorHidden = NO;
    model.bottomSeparatorColor = [UIColor redColor];
    model.cellTag = tag;
    model.useChangeObserver = YES;
    model.useCacheHeight = YES;
    return model;
}

#pragma mark - UITableViewDelegate

UITableViewDelegate_ZCP_BASE_IMP

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RandomLengthTextCellViewModel *viewModel = (RandomLengthTextCellViewModel *)[self.tableViewDataSource viewModelForRowAtIndexPath:indexPath];
    if (indexPath.row == 0) {
        viewModel.titleString = [NSString stringWithFormat:@"%@6", viewModel.titleString];
    } else if (indexPath.row == 1) {
        viewModel.titleString = [NSString stringWithFormat:@"%@牛逼", viewModel.titleString];
    } else if (indexPath.row == 2) {
        viewModel.titleString = [NSString stringWithFormat:@"%@哈", viewModel.titleString];
    } else {
        viewModel.titleString = [NSString stringWithFormat:@"%@的", viewModel.titleString];
    }
    [self.tableView reloadData];
}

#pragma mark - getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView zcp_new];
        _tableView.delegate = self;
        _tableView.dataSource = self.tableViewDataSource;
    }
    return _tableView;
}

- (ZCPTableViewSingleSectionDataSource *)tableViewDataSource {
    if (!_tableViewDataSource) {
        _tableViewDataSource = [[ZCPTableViewSingleSectionDataSource alloc] init];
    }
    return _tableViewDataSource;
}

@end
