//
//  MessageCell.h
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZCPListView/ZCPListView.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageCell : ZCPTableViewCell

@end

@interface MessageCellViewModel : ZCPTableViewCellViewModel

@property (nonatomic, assign) BOOL isNew;
@property (nonatomic, copy) NSString *avatarUrl;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, strong) NSDate *date;

@end

NS_ASSUME_NONNULL_END
