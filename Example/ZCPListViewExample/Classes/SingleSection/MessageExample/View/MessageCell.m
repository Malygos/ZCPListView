//
//  MessageCell.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "MessageCell.h"
#import <Masonry.h>
#import <YYCategories/YYCategories.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface MessageCell ()

@property (nonatomic, strong) UIView *tagBallView;
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *nickNameLabel;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIImageView *moreImageView;

@end

@implementation MessageCell

#pragma mark - override

- (void)setupContentView {
    [super setupContentView];
    [self.contentView addSubview:self.tagBallView];
    [self.contentView addSubview:self.avatarImageView];
    [self.contentView addSubview:self.nickNameLabel];
    [self.contentView addSubview:self.messageLabel];
    [self.contentView addSubview:self.dateLabel];
    [self.contentView addSubview:self.moreImageView];
    
    [self.tagBallView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(8);
        make.left.mas_equalTo(6);
        make.centerY.equalTo(self);
    }];
    [self.avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(48);
        make.left.mas_equalTo(20);
        make.centerY.equalTo(self);
    }];
    [self.nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.avatarImageView.mas_right).offset(10);
        make.top.mas_equalTo(10);
        make.height.mas_equalTo(20);
    }];
    [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nickNameLabel.mas_left);
        make.top.equalTo(self.nickNameLabel.mas_bottom).offset(5);
        make.right.mas_equalTo(-20);
    }];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(100);
        make.right.equalTo(self.moreImageView.mas_left).offset(-5);
        make.centerY.equalTo(self.nickNameLabel.mas_centerY);
    }];
    [self.moreImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-20);
        make.width.mas_equalTo(9.2);
        make.height.mas_equalTo(14.8);
        make.centerY.equalTo(self.nickNameLabel.mas_centerY);
    }];
}

- (void)updateWithViewModel:(MessageCellViewModel *)viewModel {
    [super setViewModel:viewModel];
    
    self.tagBallView.hidden = !viewModel.isNew;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:viewModel.avatarUrl?:@""] placeholderImage:[UIImage imageNamed:@"message_avatar_default"]];
    self.nickNameLabel.text = viewModel.nickName?:@"";
    self.messageLabel.text = viewModel.message?:@"";
    self.dateLabel.text = [self dateStringFromDate:viewModel.date];
    self.messageLabel.text = viewModel.message?:@"";
}

- (NSString *)dateStringFromDate:(NSDate *)date {
    NSString *dateString = [date stringWithFormat:@"yyyy-MM-dd"];
    return dateString;
}

#pragma mark - getters

- (UIView *)tagBallView {
    if (!_tagBallView) {
        _tagBallView = [[UIView alloc] init];
        _tagBallView.hidden = YES;
        _tagBallView.backgroundColor = UIColorHex(0x3678F4);
        _tagBallView.layer.cornerRadius = 4;
    }
    return _tagBallView;
}

- (UIImageView *)avatarImageView {
    if (!_avatarImageView) {
        _avatarImageView = [[UIImageView alloc] init];
        _avatarImageView.layer.cornerRadius = 24;
        _avatarImageView.layer.masksToBounds = YES;
    }
    return _avatarImageView;
}

- (UILabel *)nickNameLabel {
    if (!_nickNameLabel) {
        _nickNameLabel = [[UILabel alloc] init];
        _nickNameLabel.font = [UIFont boldSystemFontOfSize:18];
        _nickNameLabel.textColor = UIColorHex(0x000000);
    }
    return _nickNameLabel;
}

- (UILabel *)messageLabel {
    if (!_messageLabel) {
        _messageLabel = [[UILabel alloc] init];
        _messageLabel.font = [UIFont systemFontOfSize:15];
        _messageLabel.textColor = UIColorHex(0x666666);
        _messageLabel.numberOfLines = 2;
    }
    return _messageLabel;
}

- (UILabel *)dateLabel {
    if (!_dateLabel) {
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.font = [UIFont systemFontOfSize:15];
        _dateLabel.textColor = UIColorHex(0x666666);
        _dateLabel.textAlignment = NSTextAlignmentRight;
    }
    return _dateLabel;
}

- (UIImageView *)moreImageView {
    if (!_moreImageView) {
        _moreImageView = [[UIImageView alloc] init];
        _moreImageView.image = [UIImage imageNamed:@"icon_more"];
    }
    return _moreImageView;
}

@end

@implementation MessageCellViewModel

- (instancetype)init {
    if (self = [super init]) {
        self.cellClass = [MessageCell class];
        self.cellReuseIdentifier = [MessageCell cellReuseIdentifier];
        self.cellHeight = @80;
        self.bottomSeparatorHidden = NO;
        self.bottomSeparatorInset = UIEdgeInsetsMake(0, 20, 0, 0);
        self.useChangeObserver = YES;
    }
    return self;
}

+ (NSMutableSet *)causingUpdatePropertys {
    NSMutableSet *causingUpdatePropertys = [super causingUpdatePropertys];
    [causingUpdatePropertys addObjectsFromArray:@[@"isNew",
                                                  @"avatarUrl",
                                                  @"nickName",
                                                  @"message",
                                                  @"date"]];
    return causingUpdatePropertys;
}

@end
