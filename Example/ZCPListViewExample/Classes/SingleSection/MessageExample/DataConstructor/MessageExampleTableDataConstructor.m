//
//  MessageExampleTableDataConstructor.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "MessageExampleTableDataConstructor.h"
#import <MJExtension/MJExtension.h>

@implementation MessageExampleTableDataConstructor

#pragma mark - fetch data

- (void)fetchMessageDataWithSuccess:(dispatch_block_t)success {
    NSMutableArray *rawData = @[
  @{@"head": @"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1601482904,1022427523&fm=26&gp=0.jpg",
    @"message": @"在吗？",
    @"id": @"10001", @"nickname": @"张三", @"number": @"18800000000", @"timestamp": @"1585647758", @"new": @(YES)},
  @{@"head": @"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1601482904,1022427523&fm=26&gp=0.jpg",
    @"message": @"在吗？最近手头有点紧，想借2w块钱，月底肯定还你。",
    @"id": @"10002", @"nickname": @"李四", @"number": @"18800000000", @"timestamp": @"1585647758", @"new": @(YES)},
  @{@"head": @"",
    @"message": @"【流量预警】截至02月31日09时42分，您当月套餐内的国内通用流量已用545.0MB，剩余482.0MB；其他流量使用情况请点击进入 http://www.10010.com/ 查询详情。【中国联通】",
    @"id": @"10003", @"number": @"10010", @"timestamp": @"1585647758", @"new": @(YES)}].mutableCopy;
    for (int i = 0; i < 10; i++) {
        [rawData addObject:@{@"": [NSString stringWithFormat:@"200%li", (long)i], @"head": @"", @"message": @"【中国平安】恭喜您的额度提升了，给到您的预授信额已临时调高至350000元，三天有效.了解回复字母Y,退订回TD", @"number": @"1069285654172449", @"timestamp": @"1585647758", @"new": @(i < 5)}];
    }
    
    self.messageModels = [MessageModel mj_objectArrayWithKeyValuesArray:rawData];
    if (success) {
        success();
    }
}

#pragma mark - construct data

- (NSArray<ZCPTableViewCellViewModel *> *)constructMessageData {
    NSMutableArray *viewModelArray = [NSMutableArray array];
    for (int i = 0; i < self.messageModels.count; i++) {
        MessageModel *model = self.messageModels[i];
        MessageCellViewModel *viewModel = [[MessageCellViewModel alloc] init];
        viewModel.cellTag = i;
        viewModel.avatarUrl = model.avatarUrl;
        viewModel.nickName = (model.nickName.length > 0) ? model.nickName : model.phoneNumber;
        viewModel.message = model.message;
        viewModel.date = [NSDate dateWithTimeIntervalSince1970:model.timestamp];
        viewModel.isNew = model.isNew;
        [viewModelArray addObject:viewModel];
    }
    return viewModelArray;
}

@end
