//
//  MessageExampleTableDataConstructor.h
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZCPListView/ZCPListView.h>
#import "MessageModel.h"
#import "MessageCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessageExampleTableDataConstructor : NSObject

@property (nonatomic, strong) NSArray <MessageModel *>*messageModels;

#pragma mark - fetch data
- (void)fetchMessageDataWithSuccess:(dispatch_block_t)success;

#pragma mark - construct data
- (NSArray <ZCPTableViewCellViewModel *>*)constructMessageData;

@end

NS_ASSUME_NONNULL_END
