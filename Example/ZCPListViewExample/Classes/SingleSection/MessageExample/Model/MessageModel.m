//
//  MessageModel.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/1.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "MessageModel.h"
#import <MJExtension/MJExtension.h>

@implementation MessageModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{
        @"avatarUrl": @"head",
        @"messageId": @"id",
        @"nickName": @"nickname",
        @"phoneNumber": @"number",
        @"isNew": @"new"
    };
}

@end
