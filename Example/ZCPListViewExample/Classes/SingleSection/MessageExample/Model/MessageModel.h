//
//  MessageModel.h
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/1.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageModel : NSObject

@property (nonatomic, copy) NSString *messageId;
@property (nonatomic, copy) NSString *avatarUrl;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, assign) NSInteger timestamp;
@property (nonatomic, assign) BOOL isNew;

@end

NS_ASSUME_NONNULL_END
