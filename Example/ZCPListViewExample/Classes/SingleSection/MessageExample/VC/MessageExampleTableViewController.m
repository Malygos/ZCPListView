//
//  MessageExampleTableViewController.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "MessageExampleTableViewController.h"
#import "MessageExampleTableDataConstructor.h"
#import <Masonry.h>

@interface MessageExampleTableViewController () <UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZCPTableViewSingleSectionDataSource *tableViewDataSource;
@property (nonatomic, strong) MessageExampleTableDataConstructor *dataConstructor;

@end

@implementation MessageExampleTableViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self setupData];
}

#pragma mark - setup

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)setupData {
    [self.dataConstructor fetchMessageDataWithSuccess:^{
        [self reloadTableViewData];
    }];
}

#pragma mark - private

- (void)reloadTableViewData {
    [self.tableViewDataSource.cellViewModelArray removeAllObjects];
    [self.tableViewDataSource.cellViewModelArray addObjectsFromArray:[self.dataConstructor constructMessageData]];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

UITableViewDelegate_ZCP_BASE_IMP

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageCellViewModel *viewModel = (MessageCellViewModel *)[self.tableViewDataSource viewModelForRowAtIndexPath:indexPath];
    MessageModel *model = self.dataConstructor.messageModels[viewModel.cellTag];
    if (model.isNew) {
        model.isNew = NO;
        viewModel.isNew = NO;
        [self.tableView reloadData];
    }
}

#pragma mark - getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView zcp_new];
        _tableView.delegate = self;
        _tableView.dataSource = self.tableViewDataSource;
    }
    return _tableView;
}

- (ZCPTableViewSingleSectionDataSource *)tableViewDataSource {
    if (!_tableViewDataSource) {
        _tableViewDataSource = [[ZCPTableViewSingleSectionDataSource alloc] init];
    }
    return _tableViewDataSource;
}

- (MessageExampleTableDataConstructor *)dataConstructor {
    if (!_dataConstructor) {
        _dataConstructor = [[MessageExampleTableDataConstructor alloc] init];
    }
    return _dataConstructor;
}

@end
