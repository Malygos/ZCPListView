//
//  ExampleBaseViewController.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/1.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "ExampleBaseViewController.h"
#import <Masonry.h>

@interface ExampleBaseViewController ()

@property (nonatomic, strong) ZCPTableViewSingleSectionDataSource *tableViewDataSource;

@end

@implementation ExampleBaseViewController

@dynamic tableViewDataSource;

#pragma mark - override

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    self.tableView.frame = self.view.bounds;
}

- (void)constructData {
    [self.tableViewDataSource.cellViewModelArray removeAllObjects];
    for (NSDictionary *info in self.infoArray) {
        NSString *title = info[@"title"];
        ZCPTableViewSingleTitleCellViewModel *viewModel = [[ZCPTableViewSingleTitleCellViewModel alloc] init];
        viewModel.titleString = title;
        viewModel.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        viewModel.bottomSeparatorHidden = NO;
        viewModel.bottomSeparatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
        [self.tableViewDataSource.cellViewModelArray addObject:viewModel];
    }
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

UITableViewDelegate_ZCP_BASE_IMP

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *clsStr = self.infoArray[indexPath.row][@"class"];
    Class cls = NSClassFromString(clsStr);
    UIViewController *vc = [[cls alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - getters

- (NSArray *)infoArray {
    if (!_infoArray) {
        _infoArray = @[@{@"title": @"多SectionTableView", @"class": @"MultipleSectionTableViewController"},
                       @{@"title": @"单SectionTableView", @"class": @"SingleSectionTableViewController"}];
    }
    return _infoArray;
}


@end
