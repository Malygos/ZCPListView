//
//  ExampleBaseViewController.h
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/1.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZCPListView/ZCPListView.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExampleBaseViewController : ZCPTableViewController

@property (nonatomic, strong) NSArray *infoArray;

@end

NS_ASSUME_NONNULL_END
