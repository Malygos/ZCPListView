//
//  ViewController.m
//  ZCPListViewExample
//
//  Created by zcp on 2019/4/16.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

@synthesize infoArray = _infoArray;

- (NSArray *)infoArray {
    if (!_infoArray) {
        _infoArray = @[@{@"title": @"多SectionTableView", @"class": @"MultipleSectionTableViewController"},
                       @{@"title": @"单SectionTableView", @"class": @"SingleSectionTableViewController"}];
    }
    return _infoArray;
}

@end
