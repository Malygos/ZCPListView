//
//  ZCPTableViewCellViewModelCacheHeightProtocol.h
//  ZCPListView
//
//  Created by zcp on 2020/6/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// Cell 视图模型高度缓存协议
/// TableView reloadData 时会重新获取一遍所有 cell 的高度！！！而不是仅获取屏幕可视范围 cell 的高度。
/// 若有大量需要计算高度的 cell 会很耗时。所以有必要将高度做缓存处理。
@protocol ZCPTableViewCellViewModelCacheHeightProtocol <NSObject>

/// 是否使用缓存高度，默认为NO
@property (nonatomic, assign) BOOL useCacheHeight;
/// 是否需要重新计算缓存高度，初始值为YES，重新计算高度后将自动置为NO
@property (nonatomic, assign) BOOL needRecalculateCacheHeight;

/// 能够引起 Cell 高度重计算的属性
+ (NSMutableSet *)causingHeightRecalculatePropertys;

/// 能够引起 Cell 高度改变的属性值发生了改变
/// @param propertyName 属性名称
- (void)causingHeightChangePropertyHasChanged:(NSString *)propertyName;

@end

NS_ASSUME_NONNULL_END
