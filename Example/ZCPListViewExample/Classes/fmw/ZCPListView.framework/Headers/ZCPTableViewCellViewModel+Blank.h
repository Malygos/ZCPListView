//
//  ZCPTableViewCellViewModel+Blank.h
//  ZCPListView
//
//  Created by zcp on 2020/3/31.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCellViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZCPTableViewCellViewModel (Blank)

+ (instancetype)blankViewModelWithHeight:(CGFloat)cellHeight;
+ (instancetype)blankViewModelWithHeight:(CGFloat)cellHeight bgColor:(UIColor *)bgColor;

@end

NS_ASSUME_NONNULL_END
