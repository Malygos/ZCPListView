//
//  ZCPTableViewViewModelCauseChangeProtocol.h
//  ZCPListView
//
//  Created by zcp on 2020/4/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// ViewModel 变动协议
@protocol ZCPTableViewCellViewModelChangingProtocol <NSObject>

/// 是否使用属性变化监听，若为YES则仅当 causingUpdatePropertys 配置的属性发生改变时才会引起 cell 更新
@property (nonatomic, assign) BOOL useChangeObserver;
/// ViewModel 是否发生了变化
@property (nonatomic, assign, getter=isChanged) BOOL changed;

/// 能够引起 Cell 更新的属性
+ (NSMutableSet *)causingUpdatePropertys;

/// 能够引起 Cell 更新的属性值发生了改变
/// @param propertyName 属性名称
- (void)causingUpdatePropertysHasChanged:(NSString *)propertyName;

@end

NS_ASSUME_NONNULL_END
