//
//  ZCPListViewConfiguration.h
//  ZCPListView
//
//  Created by zcp on 16/1/14.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import <UIKit/UIkit.h>

/// 配置信息
@interface ZCPListViewConfiguration : NSObject

+ (instancetype)new NS_UNAVAILABLE;
+ (instancetype)sharedConfiguration;

/// Cell 默认上边线颜色
@property (nonatomic, strong) UIColor *defaultCellTopSeparatorColor;
/// Cell 默认下边线颜色
@property (nonatomic, strong) UIColor *defaultCellBottomSeparatorColor;

@end
