//
//  ZCPTableViewSectionViewModel.h
//  ZCPListView
//
//  Created by zcp on 2019/11/20.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewSectionViewModelProtocol.h"

NS_ASSUME_NONNULL_BEGIN

/// Section 视图模型
@interface ZCPTableViewSectionViewModel : NSObject <ZCPTableViewSectionViewModelProtocol, NSCopying>

@end

NS_ASSUME_NONNULL_END
