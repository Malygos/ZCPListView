//
//  ZCPTableViewSingleSectionDataSource.h
//  ZCPListView
//
//  Created by zcp on 2019/11/28.
//

#import "ZCPTableViewDataSource.h"

NS_ASSUME_NONNULL_BEGIN

/// 仅有一个 section 的 UITableView 数据源
@interface ZCPTableViewSingleSectionDataSource : ZCPTableViewDataSource

/// 禁用父类的 section array
@property (nonatomic, strong) NSMutableArray <ZCPTableViewSectionDataModel *>*sectionDataModelArray NS_UNAVAILABLE;
/// single section
@property (nonatomic, strong, readonly) ZCPTableViewSectionDataModel *singleSectionDataModel;
/// cell 视图模型数组，该属性仅为了方便使用，等同于 dataSource.singleSectionDataModel.cellViewModelArray
@property (nonatomic, strong, readonly) NSMutableArray *cellViewModelArray;

@end

NS_ASSUME_NONNULL_END
