//
//  ZCPTableViewCellViewModelSeparatorLineProtocol.h
//  ZCPListView
//
//  Created by zcp on 2020/6/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/// Cell 视图模型上下边线协议
@protocol ZCPTableViewCellViewModelSeparatorLineProtocol <NSObject>

/// 上边线是否隐藏，默认为YES
@property (nonatomic, assign) BOOL topSeparatorHidden;
/// 上边线颜色，如果传nil，则颜色默认为e4e4e4
@property (nonatomic, strong) UIColor *topSeparatorColor;
/// 上边线边距，仅left、right值有效，默认为UIEdgeInsetsZero
@property (nonatomic, assign) UIEdgeInsets topSeparatorInset;
/// 下边线是否隐藏，默认为YES
@property (nonatomic, assign) BOOL bottomSeparatorHidden;
/// 下边线颜色，如果传nil，则颜色默认为e4e4e4
@property (nonatomic, strong) UIColor *bottomSeparatorColor;
/// 下边线边距，仅left、right值有效，默认为UIEdgeInsetsZero
@property (nonatomic, assign) UIEdgeInsets bottomSeparatorInset;

@end

NS_ASSUME_NONNULL_END
