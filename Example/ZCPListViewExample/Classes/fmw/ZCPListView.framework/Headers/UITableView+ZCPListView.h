//
//  UITableView+ZCPListView.h
//  ZCPListView
//
//  Created by zcp on 2020/3/31.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITableView (ZCPListView)

+ (instancetype)zcp_new;
+ (instancetype)zcp_newWithStyle:(UITableViewStyle)style;
- (instancetype)zcp_initWithFrame:(CGRect)frame style:(UITableViewStyle)style;

@end

NS_ASSUME_NONNULL_END
