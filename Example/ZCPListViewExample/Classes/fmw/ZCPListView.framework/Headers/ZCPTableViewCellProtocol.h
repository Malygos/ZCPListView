//
//  ZCPTableViewCellProtocol.h
//  ZCPListView
//
//  Created by zcp on 2019/11/20.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCellViewModelProtocol.h"

NS_ASSUME_NONNULL_BEGIN

/// cell 协议
@protocol ZCPTableViewCellProtocol <NSObject>

/// 顶部分割线
@property (nonatomic, strong) UIView *topSeparatorView;
/// 底部分割线
@property (nonatomic, strong) UIView *bottomSeparatorView;
/// 视图模型
@property (nonatomic, strong) id<ZCPTableViewCellViewModelProtocol> viewModel;

/// 根据 viewModel 获得 Cell 高度
/// @param viewModel 视图模型
+ (CGFloat)heightForViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

/// 根据 viewModel 获得 Cell 的缓存高度
/// @param viewModel 视图模型
+ (CGFloat)cacheHeightForViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

/// 根据 viewModel 计算 Cell 的缓存高度，子类需重写该方法进行计算
/// @param viewModel 视图模型
+ (CGFloat)calculateCacheHeightForViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

/// 根据 viewmodel 实例化 Cell
/// @param viewModel 视图模型
+ (instancetype)cellForViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

/// cell 重用标记
+ (NSString *)cellReuseIdentifier;

/// 初始化 contentView 的内容，子类可重写该方法处理子视图
- (void)setupContentView;

/// 根据 viewModel 判断是否需要更新 Cell
/// @param viewModel Cell 视图模型
- (BOOL)needsUpdateAccordingToViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

/// 根据 viewModel 更新 Cell
/// @param viewModel Cell 视图模型
- (void)updateWithViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel;

@end

NS_ASSUME_NONNULL_END
