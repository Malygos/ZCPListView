//
//  ZCPTableViewSingleTitleCell.h
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import "ZCPTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

/// 单标题 cell
@interface ZCPTableViewSingleTitleCell : ZCPTableViewCell

@property (nonatomic, strong) UILabel *titleLabel;

@end

/// 单标题 cell viewmodel
@interface ZCPTableViewSingleTitleCellViewModel : ZCPTableViewCellViewModel

/// 标题
@property (nonatomic, copy, nullable) NSString *titleString;
/// 标题字体，若不设置默认为system 17
@property (nonatomic, strong, nullable) UIFont *titleFont;
/// 标题颜色，若不设置默认为 black
@property (nonatomic, strong, nullable) UIColor *titleColor;
/// 富文本标题
@property (nonatomic, strong, nullable) NSAttributedString *attributedTitleString;
/// 标题对齐方式，若不设置默认为 left
@property (nonatomic, assign) NSTextAlignment titleAlignment;
/// 标题最大行数，默认为0
@property (nonatomic, assign) NSInteger titleNumberOfLines;
/// 标题相对于contentView的外边距，默认为UIEdgeInsetsZero
@property (nonatomic, assign) UIEdgeInsets titleEdgeInsets;

@end

NS_ASSUME_NONNULL_END
