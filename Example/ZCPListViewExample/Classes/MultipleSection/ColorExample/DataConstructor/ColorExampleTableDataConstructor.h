//
//  ColorExampleTableDataConstructor.h
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZCPListView/ZCPListView.h>

NS_ASSUME_NONNULL_BEGIN

@interface ColorExampleTableDataConstructor : NSObject

@property (nonatomic, strong) NSArray *colorInfos;
- (NSArray <ZCPTableViewSectionDataModel *>*)constructColorData;

@end

NS_ASSUME_NONNULL_END
