//
//  ColorExampleTableDataConstructor.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "ColorExampleTableDataConstructor.h"
#import <YYCategories/YYCategories.h>

@implementation ColorExampleTableDataConstructor

- (NSArray<ZCPTableViewSectionDataModel *> *)constructColorData {
    NSMutableArray *dataModelArray = [NSMutableArray array];
    
    for (NSDictionary *colorInfo in self.colorInfos) {
        ZCPTableViewSectionDataModel *dataModel = [[ZCPTableViewSectionDataModel alloc] init];
        dataModel.indexTitle = colorInfo[@"title"];
        
        // header
        ZCPTableViewSingleTitleSectionViewModel *headerViewModel = [[ZCPTableViewSingleTitleSectionViewModel alloc] init];
        headerViewModel.titleString = colorInfo[@"title"];
        headerViewModel.sectionViewHeight = @(50);
        headerViewModel.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        dataModel.headerViewModel = headerViewModel;
        
        // cells
        NSArray *colors = colorInfo[@"colors"];
        for (NSString *color in colors) {
            ZCPTableViewCellViewModel *cellViewModel = [[ZCPTableViewCellViewModel alloc] init];
            cellViewModel.cellBgColor = [UIColor colorWithHexString:color];
            cellViewModel.cellHeight = @50;
            [dataModel.cellViewModelArray addObject:cellViewModel];
        }
        
        [dataModelArray addObject:dataModel];
    }
    return dataModelArray;
}

#pragma mark - getters

- (NSArray *)colorInfos {
    if (!_colorInfos) {
        _colorInfos = @[@{@"title": @"红",
                          @"colors": @[@"ffecec", @"ffd2d2", @"ffb5b5", @"ff9797",
                                       @"ff7575", @"ff5151", @"ff2d2d", @"ff0000",
                                       @"ea0000", @"ce0000", @"ae0000", @"930000",
                                       @"750000", @"600000", @"4d0000", @"2f0000"]},
                        @{@"title": @"橙",
                          @"colors": @[@"fff3ee", @"ffe6d9", @"ffdac8", @"ffcbb3",
                                       @"ffbd9d", @"ffad86", @"ff9d6f", @"ff8f59",
                                       @"ff8040", @"ff5809", @"f75000", @"d94600",
                                       @"bb3d00", @"a23400", @"842b00", @"642100"]},
                        @{@"title": @"黄",
                          @"colors": @[@"fffff4", @"ffffdf", @"ffffce", @"ffffb9",
                                       @"ffffaa", @"ffff93", @"ffff6f", @"ffff37",
                                       @"f9f900", @"e1e100", @"c4c400", @"a6a600",
                                       @"8c8c00", @"737300", @"5b5b00", @"424200"]},
                        @{@"title": @"绿",
                          @"colors": @[@"f5ffe8", @"efffd7", @"e8ffc4", @"deffac",
                                       @"d3ff93", @"ccff80", @"c2ff68", @"b7ff4a",
                                       @"a8ff24", @"9aff02", @"8cea00", @"82d900",
                                       @"73bf00", @"64a600", @"548c00", @"467500"]},
                        @{@"title": @"青",
                          @"colors": @[@"fdffff", @"ecffff", @"d9ffff", @"caffff",
                                       @"bbffff", @"a6ffff", @"80ffff", @"4dffff",
                                       @"00ffff", @"00e3e3", @"00caca", @"00aeae",
                                       @"009393", @"007979", @"005757", @"003e3e"]},
                        @{@"title": @"蓝",
                          @"colors": @[@"ecf5ff", @"d2e9ff", @"c4e1ff", @"acd6ff",
                                       @"97cbff", @"84c1ff", @"66b3ff", @"46a3ff",
                                       @"2894ff", @"0080ff", @"0072e3", @"0066cc",
                                       @"005ab5", @"004b97", @"003d79", @"000079"]},
                        @{@"title": @"紫",
                          @"colors": @[@"faf4ff", @"f1e1ff", @"f6caff", @"dcb5ff",
                                       @"d3a4ff", @"ca8eff", @"be77ff", @"b15bff",
                                       @"9f35ff", @"921aff", @"8600ff", @"6f00d2",
                                       @"5b00ae", @"4b0091", @"3a006f", @"28004d"]}];
    }
    return _colorInfos;
}

@end
