//
//  ColorExampleTableViewController.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/1.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "ColorExampleTableViewController.h"
#import "ColorExampleTableDataConstructor.h"
#import <Masonry.h>

@interface ColorExampleTableViewController () <UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZCPTableViewDataSource *tableViewDataSource;
@property (nonatomic, strong) ColorExampleTableDataConstructor *dataConstructor;

@end

@implementation ColorExampleTableViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self reloadTableViewData];
}

#pragma mark - setup

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

- (void)reloadTableViewData {
    [self.tableViewDataSource.sectionDataModelArray removeAllObjects];
    [self.tableViewDataSource.sectionDataModelArray addObjectsFromArray:[self.dataConstructor constructColorData]];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDelegate

UITableViewDelegate_ZCP_BASE_IMP

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView zcp_new];
        _tableView.delegate = self;
        _tableView.dataSource = self.tableViewDataSource;
    }
    return _tableView;
}

- (ZCPTableViewDataSource *)tableViewDataSource {
    if (!_tableViewDataSource) {
        _tableViewDataSource = [[ZCPTableViewDataSource alloc] init];
    }
    return _tableViewDataSource;
}

- (ColorExampleTableDataConstructor *)dataConstructor {
    if (!_dataConstructor) {
        _dataConstructor = [[ColorExampleTableDataConstructor alloc] init];
    }
    return _dataConstructor;
}

@end
