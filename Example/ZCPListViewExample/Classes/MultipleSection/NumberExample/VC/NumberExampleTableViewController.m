//
//  NumberExampleTableViewController.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "NumberExampleTableViewController.h"
#import "NumberExampleTableDataConstructor.h"
#import "UpdateCountViewModel.h"
#import <Masonry.h>

@interface NumberExampleTableViewController () <UITableViewDelegate, ZCPTableViewDataSourceListenerProtocol>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZCPTableViewDataSource *tableViewDataSource;
@property (nonatomic, strong) NumberExampleTableDataConstructor *dataConstructor;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, assign) BOOL useChangeObserver;

@end

@implementation NumberExampleTableViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self reloadTableViewData];
}

#pragma mark - setup

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.view addSubview:self.segmentedControl];
    [self.view addSubview:self.tableView];
    
    [self.segmentedControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(50);
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.segmentedControl.mas_bottom);
    }];
}

- (void)reloadTableViewData {
    [self.tableViewDataSource.sectionDataModelArray removeAllObjects];
    [self.tableViewDataSource.sectionDataModelArray addObjectsFromArray:[self.dataConstructor constructNumberDataWithUseChangeObserver:self.useChangeObserver]];
    [self.tableView reloadData];
}

#pragma mark - event response

- (void)segmentedControlValueChanged:(UISegmentedControl *)segmentedControl {
    if (segmentedControl.selectedSegmentIndex == 0) {
        self.useChangeObserver = NO;
    } else if (segmentedControl.selectedSegmentIndex == 1) {
        self.useChangeObserver = YES;
    }
    [self reloadTableViewData];
}

#pragma mark - UITableViewDelegate

UITableViewDelegate_ZCP_BASE_IMP

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewSingleTitleCellViewModel *viewModel = (ZCPTableViewSingleTitleCellViewModel *)[self.tableViewDataSource viewModelForRowAtIndexPath:indexPath];
    
    // bg color test
//    if (CGColorEqualToColor(viewModel.cellBgColor.CGColor, [UIColor redColor].CGColor)) {
//        viewModel.cellBgColor = [UIColor whiteColor];
//    } else {
//        viewModel.cellBgColor = [UIColor redColor];
//    }
    
    // height test
    if (viewModel.cellHeight.floatValue == 300) {
        viewModel.cellHeight = @50;
    } else {
        viewModel.cellHeight = @300;
    }
    
    // NSString test
//    if ([viewModel.titleString isEqualToString:@"click"]) {
//        viewModel.titleString = [NSString stringWithFormat:@"cell %li-%li", (long)indexPath.section, (long)indexPath.row];
//    } else {
//        viewModel.titleString = @"click";
//    }
    
    // NSNumber test
//    if (viewModel.titleAlignment == NSTextAlignmentCenter) {
//        viewModel.titleAlignment = NSTextAlignmentLeft;
//    } else {
//        viewModel.titleAlignment = NSTextAlignmentCenter;
//    }
    
    // NSObejct test
//    if (CGColorEqualToColor(viewModel.titleColor.CGColor, [UIColor redColor].CGColor)) {
//        viewModel.titleColor = nil;
//    } else {
//        viewModel.titleColor = [UIColor redColor];
//    }
    
    [tableView reloadData];
//    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - ZCPTableViewDataSourceListenerProtocol

- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource willUpdateCell:(ZCPTableViewCell *)cell withViewModel:(ZCPTableViewCellViewModel *)viewModel atIndexPath:(NSIndexPath *)indexPath {
    if ([viewModel isKindOfClass:[UpdateCountViewModel class]]) {
        UpdateCountViewModel *model = (UpdateCountViewModel *)viewModel;
        model.updateCount++;
    }
}

- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource didUpdateCell:(ZCPTableViewCell *)cell withViewModel:(ZCPTableViewCellViewModel *)viewModel atIndexPath:(NSIndexPath *)indexPath {
    if ([cell isKindOfClass:[ZCPTableViewSingleTitleCell class]]) {
        ZCPTableViewSingleTitleCell *titleCell = (ZCPTableViewSingleTitleCell *)cell;
        UpdateCountViewModel *model = (UpdateCountViewModel *)viewModel;
        titleCell.titleLabel.text = [NSString stringWithFormat:@"cell %li-%li updateCount:%li", (long)indexPath.section, (long)indexPath.row, (long)model.updateCount];
    }
}

- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource willUpdateHeader:(ZCPTableViewCell *)header withViewModel:(ZCPTableViewSectionViewModel *)viewModel inSection:(NSInteger)section {
    if ([viewModel isKindOfClass:[UpdateCountSectionViewModel class]]) {
        UpdateCountSectionViewModel *model = (UpdateCountSectionViewModel *)viewModel;
        model.updateCount++;
    }
}

- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource didUpdateHeader:(ZCPTableViewSectionView *)headerView withViewModel:(ZCPTableViewSectionViewModel *)viewModel inSection:(NSInteger)section {
    if ([headerView isKindOfClass:[ZCPTableViewSingleTitleSectionView class]]) {
        ZCPTableViewSingleTitleSectionView *view = (ZCPTableViewSingleTitleSectionView *)headerView;
        UpdateCountSectionViewModel *model = (UpdateCountSectionViewModel *)viewModel;
        view.titleLabel.text = [NSString stringWithFormat:@"header %li updateCount:%li", (long)section, (long)model.updateCount];
    }
}

- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource willUpdateFooter:(ZCPTableViewCell *)footer withViewModel:(ZCPTableViewSectionViewModel *)viewModel inSection:(NSInteger)section {
    if ([viewModel isKindOfClass:[UpdateCountSectionViewModel class]]) {
        UpdateCountSectionViewModel *model = (UpdateCountSectionViewModel *)viewModel;
        model.updateCount++;
    }
}

- (void)tableViewDataSource:(ZCPTableViewDataSource *)dataSource didUpdateFooter:(ZCPTableViewSectionView *)footerView withViewModel:(ZCPTableViewSectionViewModel *)viewModel inSection:(NSInteger)section {
    if ([footerView isKindOfClass:[ZCPTableViewSingleTitleSectionView class]]) {
        ZCPTableViewSingleTitleSectionView *view = (ZCPTableViewSingleTitleSectionView *)footerView;
        UpdateCountSectionViewModel *model = (UpdateCountSectionViewModel *)viewModel;
        view.titleLabel.text = [NSString stringWithFormat:@"footer %li updateCount:%li", (long)section, (long)model.updateCount];
    }
}

#pragma mark - getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView zcp_new];
        _tableView.delegate = self;
        _tableView.dataSource = self.tableViewDataSource;
    }
    return _tableView;
}

- (ZCPTableViewDataSource *)tableViewDataSource {
    if (!_tableViewDataSource) {
        _tableViewDataSource = [[ZCPTableViewDataSource alloc] init];
        _tableViewDataSource.listener = self;
    }
    return _tableViewDataSource;
}

- (NumberExampleTableDataConstructor *)dataConstructor {
    if (!_dataConstructor) {
        _dataConstructor = [[NumberExampleTableDataConstructor alloc] init];
    }
    return _dataConstructor;
}

- (UISegmentedControl *)segmentedControl {
    if (!_segmentedControl) {
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"No Use Change Observer", @"No Use Change Observer"]];
        _segmentedControl.selectedSegmentIndex = 0;
        [_segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentedControl;
}

@end
