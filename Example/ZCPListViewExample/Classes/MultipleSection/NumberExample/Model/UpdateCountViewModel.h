//
//  UpdateCountViewModel.h
//  ZCPListViewExample
//
//  Created by 朱超鹏(平安健康互联网健康研究院) on 2020/6/19.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZCPListView/ZCPListView.h>

NS_ASSUME_NONNULL_BEGIN

/// 更新计数模型
@interface UpdateCountViewModel : ZCPTableViewSingleTitleCellViewModel

/// cell更新的计数
@property (nonatomic, assign) NSInteger updateCount;

@end


/// 更新计数模型
@interface UpdateCountSectionViewModel : ZCPTableViewSingleTitleSectionViewModel

/// cell更新的计数
@property (nonatomic, assign) NSInteger updateCount;

@end

NS_ASSUME_NONNULL_END
