//
//  NumberExampleTableDataConstructor.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "NumberExampleTableDataConstructor.h"
#import "UpdateCountViewModel.h"
#import <YYCategories/YYCategories.h>

@implementation NumberExampleTableDataConstructor

- (NSArray<ZCPTableViewSectionDataModel *> *)constructNumberDataWithUseChangeObserver:(BOOL)useChangeObserver {
    NSMutableArray *dataModelArray = [NSMutableArray array];
    
    for (int i = 0; i < 10; i++) {
        ZCPTableViewSectionDataModel *dataModel = [[ZCPTableViewSectionDataModel alloc] init];
        dataModel.indexTitle = [NSString stringWithFormat:@"%li", (long)i];
        
        // header
        UpdateCountSectionViewModel *headerViewModel = [[UpdateCountSectionViewModel alloc] init];
        headerViewModel.titleString = [NSString stringWithFormat:@"header %li updateCount:0", (long)i];
        headerViewModel.sectionViewHeight = @(30);
        headerViewModel.sectionViewBgColor = UIColorHex(0xe5e5e5);
        dataModel.headerViewModel = headerViewModel;
        
        // footer
        UpdateCountSectionViewModel *footerViewModel = [[UpdateCountSectionViewModel alloc] init];
        footerViewModel.titleString = [NSString stringWithFormat:@"footer %li updateCount:0", (long)i];
        footerViewModel.sectionViewHeight = @(30);
        footerViewModel.sectionViewBgColor = UIColorHex(0xe5e5e5);
        dataModel.footerViewModel = footerViewModel;
        
        // cells
        for (int j = 0; j < 20; j++) {
            UpdateCountViewModel *cellViewModel = [[UpdateCountViewModel alloc] init];
            cellViewModel.titleString = [NSString stringWithFormat:@"cell %li-%li updateCount:0", (long)i, (long)j];
            cellViewModel.cellHeight = @50;
            cellViewModel.bottomSeparatorHidden = NO;
            cellViewModel.useChangeObserver = useChangeObserver;
            cellViewModel.updateCount = 0;
            [dataModel.cellViewModelArray addObject:cellViewModel];
        }
        [dataModelArray addObject:dataModel];
    }
    return dataModelArray;
}

@end
