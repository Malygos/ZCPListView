//
//  EditCell.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/8.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "EditCell.h"
#import <Masonry/Masonry.h>
#import <YYCategories/YYCategories.h>

@interface EditCell ()

@property (nonatomic, strong) UIImageView *selectImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *editButton;
@property (nonatomic, strong) UIButton *moveButton;

@end

@implementation EditCell

#pragma mark - override

- (void)setupContentView {
    [super setupContentView];
    
    self.tintColor = [UIColor redColor];
    
    [self.contentView addSubview:self.selectImageView];
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.editButton];
    [self.contentView addSubview:self.moveButton];
    
    [self.selectImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.mas_equalTo(20);
        make.left.mas_equalTo(15);
        make.centerY.equalTo(self);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.selectImageView.mas_right).offset(10);
        make.centerY.equalTo(self);
    }];
    [self.moveButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(15);
        make.right.mas_equalTo(-15);
        make.centerY.equalTo(self);
    }];
    [self.editButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.moveButton.mas_left).offset(-10);
        make.width.height.mas_equalTo(20);
        make.centerY.equalTo(self);
    }];
}

- (void)updateWithViewModel:(EditCellViewModel *)viewModel {
    [super updateWithViewModel:viewModel];
    
    self.selectImageView.image = [UIImage imageNamed:viewModel.isSelected?@"icon_select_selected":@"icon_select_normal"];
    self.titleLabel.text = viewModel.title;
}

#pragma mark - getters

- (UIImageView *)selectImageView {
    if (!_selectImageView) {
        _selectImageView = [[UIImageView alloc] init];
        _selectImageView.image = [UIImage imageNamed:@"icon_select_normal"];
    }
    return _selectImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:15];
        _titleLabel.textColor = UIColorHex(0x666666);
    }
    return _titleLabel;
}

- (UIButton *)editButton {
    if (!_editButton) {
        _editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _editButton.adjustsImageWhenHighlighted = NO;
        [_editButton setImage:[UIImage imageNamed:@"icon_edit"] forState:UIControlStateNormal];
    }
    return _editButton;
}

- (UIButton *)moveButton {
    if (!_moveButton) {
        _moveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _moveButton.adjustsImageWhenHighlighted = NO;
        [_moveButton setImage:[UIImage imageNamed:@"icon_move"] forState:UIControlStateNormal];
    }
    return _moveButton;
}

@end

@implementation EditCellViewModel

- (instancetype)init {
    if (self = [super init]) {
        self.cellClass = [EditCell class];
        self.cellReuseIdentifier = [EditCell cellReuseIdentifier];
        self.cellHeight = @60;
        self.bottomSeparatorHidden = NO;
        self.bottomSeparatorInset = UIEdgeInsetsMake(0, 45, 0, 0);
        self.cellSelectedBgColor = [UIColor greenColor];
    }
    return self;
}

@end
