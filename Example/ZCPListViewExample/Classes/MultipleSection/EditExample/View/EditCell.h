//
//  EditCell.h
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/8.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ZCPListView/ZCPListView.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditCell : ZCPTableViewCell

@end

@interface EditCellViewModel : ZCPTableViewCellViewModel

@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign, getter=isSelected) BOOL selected;

@end

NS_ASSUME_NONNULL_END
