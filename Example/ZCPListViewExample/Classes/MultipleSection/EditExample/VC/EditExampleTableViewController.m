//
//  EditExampleTableViewController.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/8.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "EditExampleTableViewController.h"
#import "EditExampleTableDataConstructor.h"
#import "EditCell.h"
#import <Masonry.h>

@interface EditExampleTableViewController () <UITableViewDelegate, ZCPTableViewDataSourceListenerProtocol>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZCPTableViewDataSource *tableViewDataSource;
@property (nonatomic, strong) EditExampleTableDataConstructor *dataConstructor;

@end

@implementation EditExampleTableViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self setupData];
}

#pragma mark - setup

- (void)setupView {
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(tapEditButton:)];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - event response

- (void)tapEditButton:(UIBarButtonItem *)button {
    if ([self.navigationItem.rightBarButtonItem.title isEqualToString:@"编辑"]) {
        self.navigationItem.rightBarButtonItem.title = @"完成";
        [self.tableView setEditing:YES animated:YES];
    } else {
        self.navigationItem.rightBarButtonItem.title = @"编辑";
        [self.tableView setEditing:NO animated:YES];
    }
}

#pragma mark - private

- (void)reloadTableViewData {
    [self.tableViewDataSource.sectionDataModelArray removeAllObjects];
    [self.tableViewDataSource.sectionDataModelArray addObjectsFromArray:[self.dataConstructor constructData]];
    [self.tableView reloadData];
}

- (void)setupData {
    [self.dataConstructor fetchDataWithSuccess:^{
        [self reloadTableViewData];
    }];
}

#pragma mark - UITableViewDelegate

UITableViewDelegate_ZCP_BASE_IMP
UITableViewDelegate_ZCP_EDIT_IMP

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.editing) {
        return;
    }
    EditCellViewModel *viewModel = (EditCellViewModel *)[self.tableViewDataSource viewModelForRowAtIndexPath:indexPath];
    viewModel.selected = !viewModel.isSelected;
    [self.tableView reloadData];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleInsert) {
        NSLog(@"[EditExample] 点击了编辑状态下左侧缩进处的Insert按钮");
    } else if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSLog(@"[EditExample] 点击了侧滑的Delete按钮");
    } else {
        NSLog(@"[EditExample] 点击了一个不知道什么按钮");
    }
}

#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_11_0
- (UISwipeActionsConfiguration *)tableView:(UITableView *)tableView leadingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}
- (UISwipeActionsConfiguration *)tableView:(UITableView *)tableView trailingSwipeActionsConfigurationForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}
#endif

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"[EditExample] willBeginEditingRowAtIndexPath %li", indexPath.row);
}

- (void)tableView:(UITableView *)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"[EditExample] didEndEditingRowAtIndexPath %li", indexPath.row);
}

#pragma mark - getters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView zcp_newWithStyle:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self.tableViewDataSource;
    }
    return _tableView;
}

- (ZCPTableViewDataSource *)tableViewDataSource {
    if (!_tableViewDataSource) {
        _tableViewDataSource = [[ZCPTableViewDataSource alloc] init];
        _tableViewDataSource.listener = self;
    }
    return _tableViewDataSource;
}

- (EditExampleTableDataConstructor *)dataConstructor {
    if (!_dataConstructor) {
        _dataConstructor = [[EditExampleTableDataConstructor alloc] init];
        _dataConstructor.tableViewRowActionHandler = ^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
            NSLog(@"[EditExample] %@", action.title);
        };
    }
    return _dataConstructor;
}

@end
