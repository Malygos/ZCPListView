//
//  EditExampleTableDataConstructor.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/8.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "EditExampleTableDataConstructor.h"
#import "EditCell.h"
#import <YYCategories/YYCategories.h>

@implementation EditExampleTableDataConstructor

#pragma mark - fetch data

- (void)fetchDataWithSuccess:(dispatch_block_t)success {
    self.rawData = @[@{@"sectionTitle": @"测试:是否可编辑",
                       @"cellTitles": @[@"1.可编辑", @"2.不可编辑"]},
                     @{@"sectionTitle": @"测试:编辑样式",
                       @"cellTitles": @[@"1.None", @"2.Delete|Insert", @"3.Insert", @"4.Delete 不设置 editActions", @"5.Delete 设置 editActions"]},
                     @{@"sectionTitle": @"测试:侧滑按钮",
                       @"cellTitles": @[@"1.默认样式侧滑按钮", @"2.自定义颜色侧滑按钮", @"3.多按钮测试", @"4.多文字测试"]},
                     @{@"sectionTitle": @"测试:编辑状态下左侧是否缩进",
                       @"cellTitles": @[@"1.可缩进", @"2.不可缩进"]}
                    ].mutableCopy;
    if (success) {
        success();
    }
}

#pragma mark - construct data

- (NSArray <ZCPTableViewSectionDataModel *>*)constructData {
    NSMutableArray *dataModelArray = [NSMutableArray array];
    
    for (NSInteger i = 0; i < self.rawData.count; i++) {
        NSDictionary *sectionInfoDict = self.rawData[i];
        ZCPTableViewSectionDataModel *dataModel = [[ZCPTableViewSectionDataModel alloc] init];
        ZCPTableViewSingleTitleSectionViewModel *headerViewModel = [[ZCPTableViewSingleTitleSectionViewModel alloc] init];
        headerViewModel.titleString = sectionInfoDict[@"sectionTitle"];
        headerViewModel.sectionViewHeight = @30;
        headerViewModel.sectionViewBgColor = UIColorHex(0xeeeeee);
        dataModel.headerViewModel = headerViewModel;
        
        NSArray *cellTitles = sectionInfoDict[@"cellTitles"];
        if (i == 0) {
            dataModel.cellViewModelArray = [self test_canEdit:cellTitles];
        }
        else if (i == 1) {
            dataModel.cellViewModelArray = [self test_editingStyle:cellTitles];
        }
        else if (i == 2) {
            dataModel.cellViewModelArray = [self test_editActions:cellTitles];
        }
        else if (i == 3) {
            dataModel.cellViewModelArray = [self test_shouldIndentWhileEditingRow:cellTitles];
        }
        
        [dataModelArray addObject:dataModel];
    }
    return dataModelArray;
}

#pragma mark - test
                     
- (NSMutableArray <ZCPTableViewCellViewModel *>*)test_canEdit:(NSArray *)titles {
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSInteger i = 0; i < titles.count; i++) {
        NSString *title = titles[i];
        EditCellViewModel *viewModel = [[EditCellViewModel alloc] init];
        viewModel.title = title;
        [result addObject:viewModel];
        
        if (i == 0) {
            viewModel.canEdit = YES;
        } else if (i == 1) {
            viewModel.canEdit = NO;
        }
    }
    return result;
}

- (NSMutableArray <ZCPTableViewCellViewModel *>*)test_editingStyle:(NSArray *)titles {
    NSMutableArray *result = [NSMutableArray array];
       
    for (NSInteger i = 0; i < titles.count; i++) {
        NSString *title = titles[i];
        EditCellViewModel *viewModel = [[EditCellViewModel alloc] init];
        viewModel.title = title;
        viewModel.canEdit = YES;
        [result addObject:viewModel];
        
        if (i == 0) {
            viewModel.editingStyle = UITableViewCellEditingStyleNone;
        } else if (i == 1) {
            viewModel.editingStyle = UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
        } else if (i == 2) {
            viewModel.editingStyle = UITableViewCellEditingStyleInsert;
        } else if (i == 3) {
            viewModel.editingStyle = UITableViewCellEditingStyleDelete;
            viewModel.deleteConfirmationButtonTitle = @"删除";
        } else if (i == 4) {
            viewModel.editingStyle = UITableViewCellEditingStyleDelete;
            viewModel.editActions = @[
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Destructive" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Default" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Normal" handler:self.tableViewRowActionHandler]
            ];
        }
    }
    return result;
}

- (NSMutableArray <ZCPTableViewCellViewModel *>*)test_editActions:(NSArray *)titles {
    NSMutableArray *result = [NSMutableArray array];
       
    for (NSInteger i = 0; i < titles.count; i++) {
        NSString *title = titles[i];
        EditCellViewModel *viewModel = [[EditCellViewModel alloc] init];
        viewModel.title = title;
        viewModel.canEdit = YES;
        viewModel.shouldIndentWhileEditingRow = NO;
        [result addObject:viewModel];
        
        if (i == 0) {
            viewModel.editActions = @[
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Destructive" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Default" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Normal" handler:self.tableViewRowActionHandler]
            ];
        }
        else if (i == 1) {
            viewModel.editActions = @[
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"编辑" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"XX" handler:self.tableViewRowActionHandler]
            ];
            viewModel.editActions[0].backgroundColor = UIColorHex(0xE25F67);
            viewModel.editActions[1].backgroundColor = UIColorHex(0xF09555);
            viewModel.editActions[2].backgroundColor = UIColorHex(0xF5C561);
        } else if (i == 2) {
            viewModel.editActions = @[
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Destructive" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Default" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Normal1" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Normal2" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Normal3" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Normal4" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Normal5" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Normal6" handler:self.tableViewRowActionHandler]
            ];
        } else if (i == 3) {
            viewModel.editActions = @[
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Destructive" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"UITableViewRowActionStyleNormal" handler:self.tableViewRowActionHandler],
                [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"多文字测试 多文字测试 多文字测试 多文字测试 多文字测试 多文字测试 多文字测试 " handler:self.tableViewRowActionHandler]
            ];
        }
    }
    return result;
}


- (NSMutableArray <ZCPTableViewCellViewModel *>*)test_shouldIndentWhileEditingRow:(NSArray *)titles {
    NSMutableArray *result = [NSMutableArray array];
       
    for (NSInteger i = 0; i < titles.count; i++) {
        NSString *title = titles[i];
        EditCellViewModel *viewModel = [[EditCellViewModel alloc] init];
        viewModel.title = title;
        viewModel.canEdit = YES;
        viewModel.cellSelectedBgColor = [UIColor greenColor];
        [result addObject:viewModel];
        
        if (i == 0) {
            viewModel.shouldIndentWhileEditingRow = YES;
        } else if (i == 1) {
            viewModel.shouldIndentWhileEditingRow = NO;
        }
    }
    return result;
}


@end
