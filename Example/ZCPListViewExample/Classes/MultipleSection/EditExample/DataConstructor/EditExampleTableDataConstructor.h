//
//  EditExampleTableDataConstructor.h
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/8.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ZCPListView/ZCPListView.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditExampleTableDataConstructor : NSObject

@property (nonatomic, strong) NSMutableArray <NSDictionary *>*rawData;
@property (nonatomic, copy) void (^tableViewRowActionHandler) (UITableViewRowAction *action, NSIndexPath *indexPath);

#pragma mark - fetch data

- (void)fetchDataWithSuccess:(nullable dispatch_block_t)success;

#pragma mark - construct data

- (NSArray <ZCPTableViewSectionDataModel *>*)constructData;

@end

NS_ASSUME_NONNULL_END
