//
//  MultipleSectionTableViewController.m
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/3/31.
//  Copyright © 2020 zcp. All rights reserved.
//

#import "MultipleSectionTableViewController.h"

@implementation MultipleSectionTableViewController

@synthesize infoArray = _infoArray;

- (NSArray *)infoArray {
    if (!_infoArray) {
        _infoArray = @[@{@"title": @"NumberExample", @"class": @"NumberExampleTableViewController"},
                       @{@"title": @"ColorExample", @"class": @"ColorExampleTableViewController"},
                       @{@"title": @"EditExample", @"class": @"EditExampleTableViewController"}];
    }
    return _infoArray;
}

@end
