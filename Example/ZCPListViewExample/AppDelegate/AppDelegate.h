//
//  AppDelegate.h
//  ZCPListViewExample
//
//  Created by zhuchaopeng06607 on 2020/4/2.
//  Copyright © 2020 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong) UIWindow *window;

@end

