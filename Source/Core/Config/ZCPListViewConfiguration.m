//
//  ZCPListViewConfiguration.m
//  ZCPListView
//
//  Created by zcp on 16/1/14.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "ZCPListViewConfiguration.h"

@implementation ZCPListViewConfiguration

+ (instancetype)sharedConfiguration {
    static dispatch_once_t onceToken;
    static ZCPListViewConfiguration *instance;
    dispatch_once(&onceToken, ^{
        instance = [[ZCPListViewConfiguration alloc] init];
    });
    return instance;
}

@end
