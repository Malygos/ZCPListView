//
//  ZCPTableViewDataSource.m
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import "ZCPTableViewDataSource.h"

@interface ZCPTableViewDataSource ()

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) NSMutableDictionary *sectionIndexForTitleIndexMap;

@end

@implementation ZCPTableViewDataSource

@synthesize sectionDataModelArray = _sectionDataModelArray;
@synthesize listener = _listener;

#pragma mark - supplement

- (CGFloat)heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *cellViewModel = [self viewModelForRowAtIndexPath:indexPath];
    Class <ZCPTableViewCellProtocol> cellClass = cellViewModel.cellClass;
    CGFloat height = [cellClass heightForViewModel:cellViewModel];
    return height;
}

- (CGFloat)heightForHeaderInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    ZCPTableViewSectionViewModel *headerViewModel = sectionDataModel.headerViewModel;
    Class <ZCPTableViewSectionViewProtocol> sectionViewClass = headerViewModel.sectionViewClass;
    CGFloat height = [sectionViewClass heightForViewModel:headerViewModel];
    if (height == 0 && self.tableView.style == UITableViewStyleGrouped) {
        return 0.001;
    }
    return height;
}

- (CGFloat)heightForFooterInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    ZCPTableViewSectionViewModel *footerViewModel = sectionDataModel.footerViewModel;
    Class <ZCPTableViewSectionViewProtocol> sectionViewClass = footerViewModel.sectionViewClass;
    CGFloat height = [sectionViewClass heightForViewModel:footerViewModel];
    if (height == 0 && self.tableView.style == UITableViewStyleGrouped) {
        return 0.001;
    }
    return height;
}

- (UITableViewCell *)cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *cellViewModel = [self viewModelForRowAtIndexPath:indexPath];
    NSString *reuseIdentifier = cellViewModel.cellReuseIdentifier;
    ZCPTableViewCell *cell = nil;
    
    if (reuseIdentifier.length > 0) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    }
    
    if (cell == nil) {
        cell = [ZCPTableViewCell cellForViewModel:cellViewModel];
    }
    
    BOOL needsUpdate = [cell needsUpdateAccordingToViewModel:cellViewModel];
    cell.viewModel = cellViewModel;
    
    if (needsUpdate) {
        if (self.listener && [self.listener respondsToSelector:@selector(tableViewDataSource:willUpdateCell:withViewModel:atIndexPath:)]) {
            [self.listener tableViewDataSource:self willUpdateCell:cell withViewModel:cellViewModel atIndexPath:indexPath];
        }
        
        [cell updateWithViewModel:cellViewModel];
        cellViewModel.changed = NO;
        
        if (self.listener && [self.listener respondsToSelector:@selector(tableViewDataSource:didUpdateCell:withViewModel:atIndexPath:)]) {
            [self.listener tableViewDataSource:self didUpdateCell:cell withViewModel:cellViewModel atIndexPath:indexPath];
        }
    }
    return cell;
}

- (UIView *)viewForHeaderInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    ZCPTableViewSectionViewModel *headerViewModel = sectionDataModel.headerViewModel;
    Class headerViewClass = sectionDataModel.headerViewModel.sectionViewClass;
    NSString *sectionViewReuseIdentifier = headerViewModel.sectionViewReuseIdentifier;
    ZCPTableViewSectionView *headerView = nil;
    
    if (sectionViewReuseIdentifier) {
        headerView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionViewReuseIdentifier];
    }
    
    if (headerView == nil && headerViewClass) {
        headerView = [[headerViewClass alloc] initWithReuseIdentifier:sectionViewReuseIdentifier];
    }
    
    if (![headerView respondsToSelector:@selector(setViewModel:)]) {
        return headerView;
    }
    
    [headerView setViewModel:headerViewModel];
    
    if (self.listener && [self.listener respondsToSelector:@selector(tableViewDataSource:willUpdateHeader:withViewModel:inSection:)]) {
        [self.listener tableViewDataSource:self willUpdateHeader:headerView withViewModel:headerViewModel inSection:section];
    }
    
    [headerView updateWithViewModel:headerViewModel];
    
    if (self.listener && [self.listener respondsToSelector:@selector(tableViewDataSource:didUpdateHeader:withViewModel:inSection:)]) {
        [self.listener tableViewDataSource:self didUpdateHeader:headerView withViewModel:headerViewModel inSection:section];
    }
    return headerView;
}

- (UIView *)viewForFooterInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    ZCPTableViewSectionViewModel *footerViewModel = sectionDataModel.footerViewModel;
    Class footerViewClass = sectionDataModel.footerViewModel.sectionViewClass;
    NSString *sectionViewReuseIdentifier = footerViewModel.sectionViewReuseIdentifier;
    ZCPTableViewSectionView *footerView = nil;
    
    if (sectionViewReuseIdentifier) {
        footerView = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionViewReuseIdentifier];
    }
    
    if (footerView == nil && footerViewClass) {
        footerView = [[footerViewClass alloc] initWithReuseIdentifier:sectionViewReuseIdentifier];
    }
    
    if (![footerView respondsToSelector:@selector(setViewModel:)]) {
        return footerView;
    }
    
    [footerView setViewModel:footerViewModel];
    
    if (self.listener && [self.listener respondsToSelector:@selector(tableViewDataSource:willUpdateFooter:withViewModel:inSection:)]) {
        [self.listener tableViewDataSource:self willUpdateFooter:footerView withViewModel:footerViewModel inSection:section];
    }
    
    [footerView updateWithViewModel:footerViewModel];
    
    if (self.listener && [self.listener respondsToSelector:@selector(tableViewDataSource:didUpdateFooter:withViewModel:inSection:)]) {
        [self.listener tableViewDataSource:self didUpdateFooter:footerView withViewModel:footerViewModel inSection:section];
    }
    return footerView;
}

- (UITableViewCellEditingStyle)editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *viewModel = [self viewModelForRowAtIndexPath:indexPath];
    UITableViewCellEditingStyle editingStyle = viewModel.editingStyle;
    return editingStyle;
}

- (NSArray<UITableViewRowAction *> *)editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *viewModel = [self viewModelForRowAtIndexPath:indexPath];
    NSArray<UITableViewRowAction *> *editActions = viewModel.editActions;
    return editActions;
}

- (NSString *)titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *viewModel = [self viewModelForRowAtIndexPath:indexPath];
    NSString *title = viewModel.deleteConfirmationButtonTitle;
    return title;
}

#pragma mark - util

- (NSInteger)numberOfSections {
    CGFloat numberOfSections = self.sectionDataModelArray.count;
    return numberOfSections;
}

- (NSInteger)numberOfRowsInSection:(NSInteger)section {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:section];
    NSInteger numberOfRows = sectionDataModel.cellViewModelArray.count;
    return numberOfRows;
}

- (ZCPTableViewSectionDataModel *)dataModelForSectionInSection:(NSInteger)section {
    if (self.sectionDataModelArray.count <= section) {
        return nil;
    }
    ZCPTableViewSectionDataModel *sectionDataModel = self.sectionDataModelArray[section];
    return sectionDataModel;
}

- (ZCPTableViewCellViewModel *)viewModelForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewSectionDataModel *sectionDataModel = [self dataModelForSectionInSection:indexPath.section];
    if (sectionDataModel.cellViewModelArray.count <= indexPath.row) {
        return nil;
    }
    ZCPTableViewCellViewModel *cellViewModel = sectionDataModel.cellViewModelArray[indexPath.row];
    return cellViewModel;
}

- (NSArray *)sectionIndexTitles {
    NSMutableArray *sectionIndexTitles = [NSMutableArray array];
    self.sectionIndexForTitleIndexMap = [NSMutableDictionary dictionary];
    NSInteger titleIndex = 0;
    
    for (int i = 0; i < self.sectionDataModelArray.count; i++) {
        ZCPTableViewSectionDataModel *dataModel = self.sectionDataModelArray[i];
        if (dataModel.indexTitle && dataModel.indexTitle.length > 0) {
            [sectionIndexTitles addObject:dataModel.indexTitle];
            [self.sectionIndexForTitleIndexMap setObject:@(i) forKey:[@(titleIndex) stringValue]];
            titleIndex++;
        }
    }
    return sectionIndexTitles;
}

- (BOOL)canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *viewModel = [self viewModelForRowAtIndexPath:indexPath];
    BOOL canEdit = viewModel.canEdit;
    return canEdit;
}

- (BOOL)canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *viewModel = [self viewModelForRowAtIndexPath:indexPath];
    BOOL canMove = viewModel.canMove;
    return canMove;
}

- (BOOL)shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCPTableViewCellViewModel *viewModel = [self viewModelForRowAtIndexPath:indexPath];
    BOOL shouldIndentWhileEditingRow = viewModel.shouldIndentWhileEditingRow;
    return shouldIndentWhileEditingRow;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    self.tableView = tableView;
    return [self numberOfSections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellForRowAtIndexPath:indexPath];
}

// index

- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    NSArray *sectionIndexTitles = [self sectionIndexTitles];
    if (sectionIndexTitles.count == 0) {
        return nil;
    }
    return sectionIndexTitles;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(nonnull NSString *)title atIndex:(NSInteger)index {
    NSInteger section = [[self.sectionIndexForTitleIndexMap objectForKey:[@(index) stringValue]] integerValue];
    return section;
}

// edit / move

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self canEditRowAtIndexPath:indexPath];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self canMoveRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (self.listener && [self.listener respondsToSelector:@selector(tableView:commitEditingStyle:forRowAtIndexPath:)]) {
        [self.listener tableView:tableView commitEditingStyle:editingStyle forRowAtIndexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    if (self.listener && [self.listener respondsToSelector:@selector(tableView:moveRowAtIndexPath:toIndexPath:)]) {
        [self.listener tableView:tableView moveRowAtIndexPath:sourceIndexPath toIndexPath:destinationIndexPath];
    }
}

#pragma mark - getters and setters

- (NSMutableArray<ZCPTableViewSectionDataModel *> *)sectionDataModelArray {
    if (!_sectionDataModelArray) {
        _sectionDataModelArray = [NSMutableArray array];
    }
    return _sectionDataModelArray;
}

@end
