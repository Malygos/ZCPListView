//
//  ZCPTableViewCellViewModelProtocol.h
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCellViewModelChangingProtocol.h"
#import "ZCPTableViewCellViewModelEditingProtocol.h"
#import "ZCPTableViewCellViewModelCacheHeightProtocol.h"
#import "ZCPTableViewCellViewModelSeparatorLineProtocol.h"
@protocol ZCPTableViewCellProtocol;

NS_ASSUME_NONNULL_BEGIN

/// Cell 视图模型协议
@protocol ZCPTableViewCellViewModelProtocol <ZCPTableViewCellViewModelChangingProtocol, ZCPTableViewCellViewModelEditingProtocol, ZCPTableViewCellViewModelCacheHeightProtocol, ZCPTableViewCellViewModelSeparatorLineProtocol>

/// Cell 的类，用于实例化 Cell，建议仅设置一次，默认为 "ZCPTableViewCell"
@property (nonatomic, strong) Class<ZCPTableViewCellProtocol> cellClass;
/// Cell 的重用标识，用于重用 Cell，建议仅设置一次，默认为 "ZCPTableViewCell"
@property (nonatomic, copy) NSString *cellReuseIdentifier;
/// 是否使用nib，若为 YES 则会读取 xib 文件创建 cell，默认为NO
@property (nonatomic, assign) BOOL useNib;

/// Cell 的高度，用于设置 cell 的高度，默认为50
@property (nonatomic, strong) NSNumber *cellHeight;
/// Cell 的宽度，用于辅助计算 cell 中其他内容的尺寸，如计算长度不定label的高度，默认为屏幕宽度
@property (nonatomic, assign) NSNumber *cellWidth;
/// Cell 的背景颜色，默认为白色
@property (nonatomic, strong) UIColor *cellBgColor;
/// Cell 的选中状态背景颜色，默认为nil，显示系统灰色
@property (nonatomic, strong) UIColor *cellSelectedBgColor;
/// Cell 的自定义字符串标记
@property (nonatomic, copy) NSString *cellType;
/// Cell 的自定义数值标记
@property (nonatomic, assign) NSInteger cellTag;
/// Cell 的响应对象，用于响应 cell 的自定义事件
@property (nonatomic, weak) id cellSelResponse;

@end

NS_ASSUME_NONNULL_END
