//
//  ZCPTableViewCellViewModel.m
//  ZCPListView
//
//  Created by zcp on 2019/11/20.
//

#import "ZCPTableViewCellViewModel.h"

@implementation ZCPTableViewCellViewModel

#pragma mark - synthesize

@synthesize cellClass               = _cellClass;
@synthesize cellReuseIdentifier     = _cellReuseIdentifier;
@synthesize cellType                = _cellType;
@synthesize cellTag                 = _cellTag;
@synthesize cellHeight              = _cellHeight;
@synthesize cellWidth               = _cellWidth;
@synthesize cellSelResponse         = _cellSelResponse;
@synthesize useNib                  = _useNib;
@synthesize cellBgColor             = _cellBgColor;
@synthesize cellSelectedBgColor     = _cellSelectedBgColor;
@synthesize topSeparatorHidden      = _topSeparatorHidden;
@synthesize topSeparatorColor       = _topSeparatorColor;
@synthesize topSeparatorInset       = _topSeparatorInset;
@synthesize bottomSeparatorHidden   = _bottomSeparatorHidden;
@synthesize bottomSeparatorColor    = _bottomSeparatorColor;
@synthesize bottomSeparatorInset    = _bottomSeparatorInset;
@synthesize canEdit                 = _canEdit;
@synthesize canMove                 = _canMove;
@synthesize editingStyle            = _editingStyle;
@synthesize editActions             = _editActions;
@synthesize deleteConfirmationButtonTitle = _deleteConfirmationButtonTitle;
@synthesize shouldIndentWhileEditingRow = _shouldIndentWhileEditingRow;
@synthesize changed                 = _changed;
@synthesize useChangeObserver       = _useChangeObserver;
@synthesize useCacheHeight          = _useCacheHeight;
@synthesize needRecalculateCacheHeight = _needRecalculateCacheHeight;

#pragma mark - init

- (instancetype)init {
    if (self = [super init]) {
        // 默认属性值
        self.cellClass                  = NSClassFromString(@"ZCPTableViewCell");
        self.cellReuseIdentifier        = @"ZCPTableViewCell";
        self.cellHeight                 = @50;
        self.cellWidth                  = @([UIScreen mainScreen].bounds.size.width);
        self.topSeparatorHidden         = YES;
        self.bottomSeparatorHidden      = YES;
        self.shouldIndentWhileEditingRow = YES;
        self.useCacheHeight             = NO;
        self.needRecalculateCacheHeight = YES;
        // 添加属性监听
        [self addPropertysValueChangeObserver];
    }
    return self;
}

- (void)dealloc {
    // 移除属性监听
    [self removePropertysValueChangeObserver];
}

#pragma mark - NSCopying

- (instancetype)copyWithZone:(NSZone *)zone {
    ZCPTableViewCellViewModel *newViewModel = [[ZCPTableViewCellViewModel alloc] init];
    newViewModel.cellClass                  = self.cellClass;
    newViewModel.cellReuseIdentifier        = self.cellReuseIdentifier;
    newViewModel.cellType                   = self.cellType;
    newViewModel.cellTag                    = self.cellTag;
    newViewModel.cellHeight                 = self.cellHeight.copy;
    newViewModel.cellWidth                  = self.cellWidth;
    newViewModel.cellSelResponse            = self.cellSelResponse;
    newViewModel.useNib                     = self.useNib;
    newViewModel.cellBgColor                = self.cellBgColor.copy;
    newViewModel.cellSelectedBgColor        = self.cellSelectedBgColor.copy;
    newViewModel.topSeparatorHidden         = self.topSeparatorHidden;
    newViewModel.topSeparatorColor          = self.topSeparatorColor.copy;
    newViewModel.topSeparatorInset          = self.topSeparatorInset;
    newViewModel.bottomSeparatorHidden      = self.bottomSeparatorHidden;
    newViewModel.bottomSeparatorColor       = self.bottomSeparatorColor.copy;
    newViewModel.bottomSeparatorInset       = self.bottomSeparatorInset;
    newViewModel.canEdit                    = self.canEdit;
    newViewModel.canMove                    = self.canMove;
    newViewModel.editingStyle               = self.editingStyle;
    newViewModel.editActions                = self.editActions;
    newViewModel.deleteConfirmationButtonTitle = self.deleteConfirmationButtonTitle;
    newViewModel.shouldIndentWhileEditingRow = self.shouldIndentWhileEditingRow;
    newViewModel.changed                    = self.changed;
    newViewModel.useChangeObserver          = self.useChangeObserver;
    newViewModel.useCacheHeight             = self.useCacheHeight;
    newViewModel.needRecalculateCacheHeight = self.needRecalculateCacheHeight;
    return newViewModel;
}

#pragma mark - ZCPTableViewCellViewModelChangingProtocol

- (void)setChanged:(BOOL)changed {
    if (self.useChangeObserver) {
        _changed = changed;
    } else {
        _changed = YES;
    }
}

+ (NSMutableSet *)causingUpdatePropertys {
    NSMutableSet *causingUpdatePropertys = [NSMutableSet setWithObjects:
                                            @"cellHeight",
                                            @"cellBgColor",
                                            @"cellSelectedBgColor",
                                            @"topSeparatorHidden",
                                            @"topSeparatorColor",
                                            @"topSeparatorInset",
                                            @"bottomSeparatorHidden",
                                            @"bottomSeparatorColor",
                                            @"bottomSeparatorInset", nil];
    return causingUpdatePropertys;
}

- (void)causingUpdatePropertysHasChanged:(NSString *)propertyName {
    self.changed = YES;
}

#pragma mark - ZCPTableViewHeightCacheProtocol

+ (NSMutableSet *)causingHeightRecalculatePropertys {
    return [NSMutableSet set];
}

- (void)causingHeightChangePropertyHasChanged:(NSString *)propertyName {
    self.needRecalculateCacheHeight = YES;
}

#pragma mark - observer

- (void)addPropertysValueChangeObserver {
    NSMutableSet <NSString *>*set = [NSMutableSet set];
    [set unionSet:[self.class causingUpdatePropertys]];
    [set unionSet:[self.class causingHeightRecalculatePropertys]];
    
    [set enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, BOOL * _Nonnull stop) {
        [self addObserver:self forKeyPath:obj options:NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew context:nil];
    }];
}

- (void)removePropertysValueChangeObserver {
    NSMutableSet <NSString *>*set = [NSMutableSet set];
    [set unionSet:[self.class causingUpdatePropertys]];
    [set unionSet:[self.class causingHeightRecalculatePropertys]];
    
    [set enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, BOOL * _Nonnull stop) {
        [self removeObserver:self forKeyPath:obj];
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    id oldValue = change[NSKeyValueChangeOldKey];
    id newValue = change[NSKeyValueChangeNewKey];
    
    if ([self.class judgeOldValue:oldValue isEqualToNewValue:newValue]) {
        return;
    }
    
    if ([[self.class causingUpdatePropertys] containsObject:keyPath]) {
        [self causingUpdatePropertysHasChanged:keyPath];
    }
    if ([[self.class causingHeightRecalculatePropertys] containsObject:keyPath]) {
        [self causingHeightChangePropertyHasChanged:keyPath];
    }
}

+ (BOOL)judgeOldValue:(id)oldValue isEqualToNewValue:(id)newValue {
    if ([oldValue isKindOfClass:[NSNull class]] && [newValue isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([oldValue isKindOfClass:[NSNull class]] || [newValue isKindOfClass:[NSNull class]]) {
        return NO;
    }
    if ([oldValue isKindOfClass:[NSString class]] || [newValue isKindOfClass:[NSString class]]) {
        return [oldValue isEqualToString:newValue];
    }
    if ([oldValue isKindOfClass:[NSNumber class]] || [newValue isKindOfClass:[NSNumber class]]) {
        return [oldValue isEqualToNumber:newValue];
    }
    if (oldValue || newValue) {
        return [oldValue isEqual:newValue];
    }
    return YES;
}

@end
