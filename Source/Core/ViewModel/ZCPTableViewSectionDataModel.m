//
//  ZCPTableViewSectionDataModel.m
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import "ZCPTableViewSectionDataModel.h"

@implementation ZCPTableViewSectionDataModel

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    ZCPTableViewSectionDataModel *newDataModel = [[ZCPTableViewSectionDataModel alloc] init];
    newDataModel.headerViewModel = self.headerViewModel.copy;
    newDataModel.footerViewModel = self.footerViewModel.copy;
    NSMutableArray *cellViewModelArray = [NSMutableArray array];
    for (ZCPTableViewCellViewModel *cellViewModel in self.cellViewModelArray) {
        [cellViewModelArray addObject:cellViewModel.copy];
    }
    newDataModel.cellViewModelArray = cellViewModelArray;
    return newDataModel;
}

#pragma mark - getters

- (ZCPTableViewSectionViewModel *)headerViewModel {
    if (!_headerViewModel) {
        _headerViewModel = [[ZCPTableViewSectionViewModel alloc] init];
    }
    return _headerViewModel;
}

- (ZCPTableViewSectionViewModel *)footerViewModel {
    if (!_footerViewModel) {
        _footerViewModel = [[ZCPTableViewSectionViewModel alloc] init];
    }
    return _footerViewModel;
}

- (NSMutableArray<ZCPTableViewCellViewModel *> *)cellViewModelArray {
    if (!_cellViewModelArray) {
        _cellViewModelArray = [NSMutableArray array];
    }
    return _cellViewModelArray;
}

@end
