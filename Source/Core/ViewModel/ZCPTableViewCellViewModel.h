//
//  ZCPTableViewCellViewModel.h
//  ZCPListView
//
//  Created by zcp on 2019/11/20.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewCellViewModelProtocol.h"

NS_ASSUME_NONNULL_BEGIN

/// Cell 视图模型
@interface ZCPTableViewCellViewModel : NSObject <ZCPTableViewCellViewModelProtocol, NSCopying>

@end

NS_ASSUME_NONNULL_END
