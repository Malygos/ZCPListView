//
//  ZCPTableViewCellViewModelEditingProtocol.h
//  ZCPListView
//
//  Created by zcp on 2020/6/22.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// Cell 视图模型编辑相关协议
@protocol ZCPTableViewCellViewModelEditingProtocol <NSObject>

/// 是否可以左滑编辑，默认为NO
@property (nonatomic, assign) BOOL canEdit;
/// 是否可以移动，默认为NO
@property (nonatomic, assign) BOOL canMove;
/// 编辑状态下的样式，默认为 UITableViewCellEditingStyleNone，该属性仅在 canEdit 为 YES 时生效。
/// ps：None 编辑状态下左侧不显示内容；Delete 编辑状态下左侧显示红色减号⛔️；Insert 编辑状态下左侧显示绿色加号❎；Delete | Insert 编辑状态下左侧显示未选中圆圈🔘
@property (nonatomic, assign) UITableViewCellEditingStyle editingStyle;
/// 侧滑编辑按钮数组，默认为nil，该属性仅在 canEdit 为 YES 时生效
@property (nonatomic, strong) NSArray <UITableViewRowAction *>*editActions;
/// 侧滑编辑按钮仅系统删除按钮时的标题，默认为nil。
/// 该属性仅在 editingStyle 为 UITableViewCellEditingStyleDelete 且 editActions 为 nil 时生效。
/// 该属性与 editActions 互斥，即当 editActions 有值时该属性无效。不建议使用该属性，推荐使用 editActions。
@property (nonatomic, copy) NSString *deleteConfirmationButtonTitle;
/// 编辑状态下左侧是否缩进，默认为YES，该属性仅在 canEdit 为 YES 时生效。
@property (nonatomic, assign) BOOL shouldIndentWhileEditingRow;

@end

NS_ASSUME_NONNULL_END
