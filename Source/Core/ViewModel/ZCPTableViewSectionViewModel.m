//
//  ZCPTableViewSectionViewModel.m
//  ZCPListView
//
//  Created by zcp on 2019/11/20.
//

#import "ZCPTableViewSectionViewModel.h"

@implementation ZCPTableViewSectionViewModel

@synthesize sectionViewClass = _sectionViewClass;
@synthesize sectionViewReuseIdentifier = _sectionViewReuseIdentifier;
@synthesize useNib = _useNib;
@synthesize sectionViewHeight = _sectionViewHeight;
@synthesize sectionViewBgColor = _sectionViewBgColor;

#pragma mark - init

- (instancetype)init {
    if (self = [super init]) {
        self.sectionViewClass           = NSClassFromString(@"ZCPTableViewSectionView");
        self.sectionViewReuseIdentifier = @"ZCPTableViewSectionView";
        self.sectionViewHeight          = @0;
        self.sectionViewBgColor         = nil;
    }
    return self;
}

#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {
    ZCPTableViewSectionViewModel *newViewModel = [[ZCPTableViewSectionViewModel alloc] init];
    newViewModel.sectionViewClass = self.sectionViewClass;
    newViewModel.sectionViewReuseIdentifier = self.sectionViewReuseIdentifier.copy;
    newViewModel.useNib = self.useNib;
    newViewModel.sectionViewHeight = self.sectionViewHeight.copy;
    newViewModel.sectionViewBgColor = self.sectionViewBgColor.copy;
    return newViewModel;
}

@end
