//
//  ZCPTableViewSectionViewProtocol.h
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import <Foundation/Foundation.h>
#import "ZCPTableViewSectionViewModelProtocol.h"

NS_ASSUME_NONNULL_BEGIN

/// Section 视图协议
@protocol ZCPTableViewSectionViewProtocol <NSObject>

/// 视图模型
@property (nonatomic, strong) id<ZCPTableViewSectionViewModelProtocol> viewModel;

/// 根据 viewModel 获得 SectionView 高度
/// @param viewModel 视图模型
+ (CGFloat)heightForViewModel:(id<ZCPTableViewSectionViewModelProtocol>)viewModel;

/// 根据 viewModel 实例化 SectionView
/// @param viewModel 视图模型
+ (instancetype)sectionViewForViewModel:(id<ZCPTableViewSectionViewModelProtocol>)viewModel;

/// SectionView 重用标记
+ (NSString *)sectionViewReuseIdentifier;

/// 初始化 contentView 的内容
- (void)setupContentView;

/// 根据 viewModel 更新 Section
/// @param viewModel Section 视图模型
- (void)updateWithViewModel:(id<ZCPTableViewSectionViewModelProtocol>)viewModel;

@end

NS_ASSUME_NONNULL_END
