//
//  ZCPTableViewSectionView.m
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import "ZCPTableViewSectionView.h"

@implementation ZCPTableViewSectionView

@synthesize viewModel = _viewModel;

#pragma mark - init

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self setupSectionView];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSectionView];
}

- (void)setupSectionView {
    self.clipsToBounds = NO;
    self.contentView.clipsToBounds = NO;
    self.backgroundView = [[UIView alloc] init];
    // 系统默认的section颜色为e5e5e5，但是白色比较常用
    self.backgroundView.backgroundColor = [UIColor whiteColor];
    [self setupContentView];
}

#pragma mark - ZCPTableViewSectionViewProtocol

+ (CGFloat)heightForViewModel:(id<ZCPTableViewSectionViewModelProtocol>)viewModel {
    if (viewModel.sectionViewHeight) {
        CGFloat height = viewModel.sectionViewHeight.floatValue;
        return height;
    }
    return 0;
}

+ (instancetype)sectionViewForViewModel:(id<ZCPTableViewSectionViewModelProtocol>)viewModel {
    ZCPTableViewSectionView *sectionView  = nil;
    Class sectionViewClass = viewModel.sectionViewClass;
    NSString *reuseIdentifier = viewModel.sectionViewReuseIdentifier;
    
    if (viewModel.useNib) {
        UINib *nib = [UINib nibWithNibName:NSStringFromClass(sectionViewClass) bundle:[NSBundle bundleForClass:self]];
        sectionView = [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
    } else {
        sectionView = [[sectionViewClass alloc] initWithReuseIdentifier:reuseIdentifier];
    }
    return sectionView;
}

+ (NSString *)sectionViewReuseIdentifier {
    return NSStringFromClass(self);
}

- (void)setupContentView {
    
}

- (void)updateWithViewModel:(id<ZCPTableViewSectionViewModelProtocol>)viewModel {
    if (viewModel.sectionViewBgColor) {
        self.backgroundView.backgroundColor = viewModel.sectionViewBgColor;
    }
}

@end
