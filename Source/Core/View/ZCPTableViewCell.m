//
//  ZCPTableViewCell.m
//  ZCPListView
//
//  Created by zcp on 16/1/14.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "ZCPTableViewCell.h"
#import "ZCPListViewConfiguration.h"

#define UIColorE4E4E4 [UIColor colorWithRed:228/255.0 green:228/255.0 blue:228/255.0 alpha:1.0]

@implementation ZCPTableViewCell

#pragma mark - synthesize

@synthesize topSeparatorView    = _topSeparatorView;
@synthesize bottomSeparatorView = _bottomSeparatorView;
@synthesize viewModel           = _viewModel;

#pragma mark - init

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupCell];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupCell];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGSize cellSize = self.frame.size;
    CGFloat onePixel = 1.0/[UIScreen mainScreen].scale;
    
    if (!self.viewModel.topSeparatorHidden) {
        CGFloat left = self.viewModel.bottomSeparatorInset.left;
        CGFloat width = cellSize.width - left - self.viewModel.topSeparatorInset.right;
        self.topSeparatorView.frame = CGRectMake(left, 0, width, onePixel);
    }
    if (!self.viewModel.bottomSeparatorHidden) {
        CGFloat left = self.viewModel.bottomSeparatorInset.left;
        CGFloat width = self.frame.size.width - left - self.viewModel.bottomSeparatorInset.right;
        self.bottomSeparatorView.frame = CGRectMake(left, cellSize.height - onePixel, width, onePixel);
    }
}

#pragma mark - setup

- (void)setupCell {
    self.selectionStyle = UITableViewCellSelectionStyleDefault;
    self.accessoryType = UITableViewCellAccessoryNone;
    self.clipsToBounds = NO;
    self.contentView.clipsToBounds = NO;
    // FIXME: 这里还是没搞明白，需要再研究一下，若这样写的话就没有默认的点击高亮样式了，若注释则设置color不生效
    self.selectedBackgroundView = [UIView new];
    self.multipleSelectionBackgroundView = [UIView new];
    [self addSubview:self.topSeparatorView];
    [self addSubview:self.bottomSeparatorView];
    
    [self setupContentView];
    
    [self bringSubviewToFront:self.topSeparatorView];
    [self bringSubviewToFront:self.bottomSeparatorView];
}

#pragma mark - ZCPTableViewCellProtocol

+ (CGFloat)heightForViewModel:(ZCPTableViewCellViewModel *)viewModel {
    CGFloat height = 0;
    if (viewModel.useCacheHeight) {
        height = [self cacheHeightForViewModel:viewModel];
    }
    if (viewModel.cellHeight) {
        height = viewModel.cellHeight.floatValue;
    }
    return height;
}

+ (CGFloat)cacheHeightForViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel {
    if (viewModel.needRecalculateCacheHeight) {
        CGFloat cacheHeight = [self calculateCacheHeightForViewModel:viewModel];
        viewModel.cellHeight = @(cacheHeight);
        viewModel.needRecalculateCacheHeight = NO;
    }
    return viewModel.cellHeight.floatValue;
}

+ (CGFloat)calculateCacheHeightForViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel {
    // Subclass override required
    if (viewModel.cellHeight) {
        return viewModel.cellHeight.floatValue;
    }
    return 0;
}

+ (instancetype)cellForViewModel:(ZCPTableViewCellViewModel *)viewModel {
    ZCPTableViewCell *cell  = nil;
    Class cellClass = viewModel.cellClass;
    NSString *reuseIdentifier = viewModel.cellReuseIdentifier;
    
    if (viewModel.useNib) {
        UINib *nib = [UINib nibWithNibName:NSStringFromClass(cellClass) bundle:[NSBundle bundleForClass:self]];
        cell = [[nib instantiateWithOwner:nil options:nil] objectAtIndex:0];
    } else {
        cell = [[cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    }
    return cell;
}

+ (NSString *)cellReuseIdentifier {
    return NSStringFromClass(self);
}

- (void)setupContentView {
    // Subclass override required
}

- (BOOL)needsUpdateAccordingToViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel {
    if (self.viewModel == nil ||
        self.viewModel != viewModel ||
        (self.viewModel == viewModel && viewModel.isChanged)) {
        return YES;
    }
    return NO;
}

- (void)updateWithViewModel:(id<ZCPTableViewCellViewModelProtocol>)viewModel {
    // background color
    UIColor *cellBgColor = [UIColor whiteColor];
    if (viewModel.cellBgColor && [viewModel.cellBgColor isKindOfClass:[UIColor class]]) {
        cellBgColor = viewModel.cellBgColor;
    }
    if (self.backgroundView) {
        self.backgroundView.backgroundColor = cellBgColor;
    } else {
        self.backgroundColor = cellBgColor;
    }
    
    // selected background color
    UIColor *cellSelectedBgColor = nil;
    if (viewModel.cellSelectedBgColor && [viewModel.cellSelectedBgColor isKindOfClass:[UIColor class]]) {
        cellSelectedBgColor = viewModel.cellSelectedBgColor;
    }
    if (self.selectedBackgroundView) {
        self.selectedBackgroundView.backgroundColor = cellSelectedBgColor;
    }
    if (self.multipleSelectionBackgroundView) {
        self.multipleSelectionBackgroundView.backgroundColor = cellSelectedBgColor;
    }
    
    // top and bottom separator
    if (!viewModel.topSeparatorHidden) {
        if (viewModel.topSeparatorColor && [viewModel.topSeparatorColor isKindOfClass:[UIColor class]]) {
            self.topSeparatorView.backgroundColor = viewModel.topSeparatorColor;
        } else if ([ZCPListViewConfiguration sharedConfiguration].defaultCellTopSeparatorColor) {
            self.topSeparatorView.backgroundColor = [ZCPListViewConfiguration sharedConfiguration].defaultCellTopSeparatorColor;
        } else {
            self.topSeparatorView.backgroundColor = UIColorE4E4E4;
        }
    }
    if (!viewModel.bottomSeparatorHidden) {
        if (viewModel.bottomSeparatorColor && [viewModel.bottomSeparatorColor isKindOfClass:[UIColor class]]) {
            self.bottomSeparatorView.backgroundColor = viewModel.bottomSeparatorColor;
        } else if ([ZCPListViewConfiguration sharedConfiguration].defaultCellBottomSeparatorColor) {
            self.bottomSeparatorView.backgroundColor = [ZCPListViewConfiguration sharedConfiguration].defaultCellBottomSeparatorColor;
        } else {
            self.bottomSeparatorView.backgroundColor = UIColorE4E4E4;
        }
    }
    self.topSeparatorView.hidden = viewModel.topSeparatorHidden;
    self.bottomSeparatorView.hidden = viewModel.bottomSeparatorHidden;
}

#pragma mark - getters

- (UIView *)topSeparatorView {
    if (!_topSeparatorView) {
        _topSeparatorView = [[UIView alloc] init];
    }
    return _topSeparatorView;
}

- (UIView *)bottomSeparatorView {
    if (!_bottomSeparatorView) {
        _bottomSeparatorView = [[UIView alloc] init];
    }
    return _bottomSeparatorView;
}

@end
