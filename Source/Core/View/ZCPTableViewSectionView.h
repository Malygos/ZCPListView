//
//  ZCPTableViewSectionView.h
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import <UIKit/UIKit.h>
#import "ZCPTableViewSectionViewProtocol.h"
#import "ZCPTableViewSectionViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZCPTableViewSectionView : UITableViewHeaderFooterView <ZCPTableViewSectionViewProtocol>

@end

NS_ASSUME_NONNULL_END
