//
//  ZCPTableViewSingleTitleSectionView.m
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import "ZCPTableViewSingleTitleSectionView.h"

@implementation ZCPTableViewSingleTitleSectionView

#pragma mark - override

- (void)setupContentView {
    [self.contentView addSubview:self.titleLabel];
}

- (void)updateWithViewModel:(ZCPTableViewSingleTitleSectionViewModel *)viewModel {
    [super updateWithViewModel:viewModel];
    
    // 这句代码会清掉text的相关信息，text、font、color等
    self.titleLabel.attributedText = nil;
    
    // 设置基本的text样式
    self.titleLabel.text = (viewModel.titleString.length > 0) ? viewModel.titleString : @"";
    self.titleLabel.font = viewModel.titleFont;
    self.titleLabel.textColor = viewModel.titleColor;
    self.titleLabel.textAlignment = viewModel.titleAlignment;
    
    // 当有富文本时会根据富文本中的内容覆盖对应的信息，text、font、color等。如富文本中未设置某个样式时，则会使用基本样式
    if (viewModel.attributedTitleString) {
        self.titleLabel.attributedText = viewModel.attributedTitleString;
    }
    
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if ([self.viewModel isKindOfClass:[ZCPTableViewSingleTitleSectionViewModel class]]) {
        ZCPTableViewSingleTitleSectionViewModel *vm = (ZCPTableViewSingleTitleSectionViewModel *)self.viewModel;
        UIEdgeInsets insets = vm.titleEdgeInsets;
        self.titleLabel.frame = CGRectMake(insets.left, insets.top, self.frame.size.width - insets.left - insets.right, self.frame.size.height - insets.top - insets.bottom);
    }
}

#pragma mark - getters

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.textColor = [UIColor blackColor];
    }
    return _titleLabel;
}

@end

@implementation ZCPTableViewSingleTitleSectionViewModel

- (instancetype)init {
    if (self = [super init]) {
        self.sectionViewClass = [ZCPTableViewSingleTitleSectionView class];
        self.sectionViewReuseIdentifier = [ZCPTableViewSingleTitleSectionView sectionViewReuseIdentifier];
        self.sectionViewHeight = @20;
    }
    return self;
}

@end
