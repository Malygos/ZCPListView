//
//  ZCPTableViewCellViewModel+Blank.m
//  ZCPListView
//
//  Created by zcp on 2020/3/31.
//

#import "ZCPTableViewCellViewModel+Blank.h"

@implementation ZCPTableViewCellViewModel (Blank)

+ (instancetype)blankViewModelWithHeight:(CGFloat)cellHeight {
    return [self blankViewModelWithHeight:cellHeight bgColor:[UIColor whiteColor]];
}

+ (instancetype)blankViewModelWithHeight:(CGFloat)cellHeight bgColor:(UIColor *)bgColor {
    ZCPTableViewCellViewModel *viewModel = [[ZCPTableViewCellViewModel alloc] init];
    viewModel.cellHeight = @(cellHeight);
    viewModel.cellBgColor = bgColor;
    return viewModel;
}

@end
