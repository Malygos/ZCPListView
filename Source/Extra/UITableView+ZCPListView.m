//
//  UITableView+ZCPListView.m
//  ZCPListView
//
//  Created by zcp on 2020/3/31.
//

#import "UITableView+ZCPListView.h"

@implementation UITableView (ZCPListView)

+ (instancetype)zcp_new {
    return [UITableView zcp_newWithStyle:UITableViewStylePlain];
}

+ (instancetype)zcp_newWithStyle:(UITableViewStyle)style {
    return [[UITableView alloc] zcp_initWithFrame:CGRectZero style:style];
}

- (instancetype)zcp_initWithFrame:(CGRect)frame style:(UITableViewStyle)style {
    if ([self initWithFrame:frame style:style]) {
        self.backgroundColor = [UIColor whiteColor];
        self.backgroundView = nil;
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.estimatedRowHeight = 0;
        self.estimatedSectionHeaderHeight = 0;
        self.estimatedSectionFooterHeight = 0;
    }
    return self;
}

@end
