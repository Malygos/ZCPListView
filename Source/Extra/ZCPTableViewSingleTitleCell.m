//
//  ZCPTableViewSingleTitleCell.m
//  ZCPListView
//
//  Created by zcp on 2019/11/21.
//

#import "ZCPTableViewSingleTitleCell.h"

@implementation ZCPTableViewSingleTitleCell

#pragma mark - override

- (void)setupContentView {
    [self.contentView addSubview:self.titleLabel];
}

- (void)updateWithViewModel:(ZCPTableViewSingleTitleCellViewModel *)viewModel {
    [super updateWithViewModel:viewModel];
    
    // 这句代码会清掉text的相关信息，text、font、color等
    self.titleLabel.attributedText = nil;
    
    // 设置基本text样式
    self.titleLabel.text = (viewModel.titleString.length > 0) ? viewModel.titleString : @"";
    self.titleLabel.font = viewModel.titleFont;
    self.titleLabel.textColor = viewModel.titleColor;
    self.titleLabel.textAlignment = viewModel.titleAlignment;
    self.titleLabel.numberOfLines = viewModel.titleNumberOfLines;
    
    // 当有富文本时会根据富文本中的内容覆盖对应的信息，text、font、color等。如富文本中未设置某个样式时，则会使用基本样式
    if (viewModel.attributedTitleString) {
        self.titleLabel.attributedText = viewModel.attributedTitleString;
    }
    [self setNeedsLayout];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if ([self.viewModel isKindOfClass:[ZCPTableViewSingleTitleCellViewModel class]]) {
        ZCPTableViewSingleTitleCellViewModel *vm = (ZCPTableViewSingleTitleCellViewModel *)self.viewModel;
        UIEdgeInsets insets = vm.titleEdgeInsets;
        self.titleLabel.frame = CGRectMake(insets.left, insets.top, self.frame.size.width - insets.left - insets.right, self.frame.size.height - insets.top - insets.bottom);
    }
}

#pragma mark - protocol

+ (CGFloat)calculateCacheHeightForViewModel:(ZCPTableViewSingleTitleCellViewModel *)viewModel {
    ZCPTableViewSingleTitleCellViewModel *vm = (ZCPTableViewSingleTitleCellViewModel *)viewModel;
    UIEdgeInsets insets = vm.titleEdgeInsets;
    CGFloat cellWidth = vm.cellWidth.floatValue;
    CGSize titleMaxSize = CGSizeMake(cellWidth - insets.left - insets.right, MAXFLOAT);
    CGFloat titleHeight = 0;
    
    if (vm.attributedTitleString) {
        titleHeight = [vm.attributedTitleString boundingRectWithSize:titleMaxSize options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin context:nil].size.height;
    } else if (vm.titleString) {
        UIFont *titleFont = vm.titleFont ? : [UIFont systemFontOfSize:17];
        titleHeight = [vm.titleString boundingRectWithSize:titleMaxSize options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: titleFont} context:nil].size.height;
    }
    
    CGFloat height = titleHeight + insets.top + insets.bottom;
    return height;
}

#pragma mark - getters and setters

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:17];
        _titleLabel.textColor = [UIColor blackColor];
    }
    return _titleLabel;
}

@end

@implementation ZCPTableViewSingleTitleCellViewModel

- (instancetype)init {
    if (self = [super init]) {
        self.cellClass = [ZCPTableViewSingleTitleCell class];
        self.cellReuseIdentifier = [ZCPTableViewSingleTitleCell cellReuseIdentifier];
        self.cellHeight = @50;
    }
    return self;
}

+ (NSMutableSet *)causingUpdatePropertys {
    NSArray *newPropertys = @[@"titleString",
                              @"titleFont",
                              @"titleColor",
                              @"attributedTitleString",
                              @"titleAlignment",
                              @"titleNumberOfLines",
                              @"titleEdgeInsets"];
    NSMutableSet *causingUpdatePropertys = [super causingUpdatePropertys];
    [causingUpdatePropertys addObjectsFromArray:newPropertys];
    return causingUpdatePropertys;
}

+ (NSMutableSet *)causingHeightRecalculatePropertys {
    NSArray *newPropertys = @[@"titleString",
                              @"attributedTitleString",
                              @"titleFont"];
    NSMutableSet *causingHeightRecalculatePropertys = [super causingHeightRecalculatePropertys];
    [causingHeightRecalculatePropertys addObjectsFromArray:newPropertys];
    return causingHeightRecalculatePropertys;
}

@end
