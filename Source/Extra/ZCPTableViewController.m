//
//  ZCPTableViewController.m
//  ZCPListView
//
//  Created by zcp on 16/9/21.
//  Copyright © 2016年 zcp. All rights reserved.
//

#import "ZCPTableViewController.h"
#import "UITableView+ZCPListView.h"

@interface ZCPTableViewController ()

@property (nonatomic, strong, readwrite) UITableView *tableView;
@property (nonatomic, strong, readwrite) ZCPTableViewDataSource *tableViewDataSource;
@property (nonatomic, assign, readwrite) BOOL isSingleSectionTableView;

@end

@implementation ZCPTableViewController

#pragma mark - life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.isSingleSectionTableView = YES;
    [self.view addSubview:self.tableView];
    [self constructData];
    [self.tableView reloadData];
}

#pragma mark - public

+ (instancetype)instanceSingleSectionTableViewController {
    ZCPTableViewController *instance = [[ZCPTableViewController alloc] init];
    instance.isSingleSectionTableView = YES;
    return instance;
}

+ (instancetype)instanceMultiSectionTableViewController {
    ZCPTableViewController *instance = [[ZCPTableViewController alloc] init];
    instance.isSingleSectionTableView = NO;
    return instance;
}

- (void)constructData {
}

#pragma mark - UITableViewDelegate

UITableViewDelegate_ZCP_BASE_IMP

#pragma mark - getters and setters

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [UITableView zcp_new];
        _tableView.dataSource = self.tableViewDataSource;
        _tableView.delegate = self;
    }
    return _tableView;
}

- (ZCPTableViewDataSource *)tableViewDataSource {
    if (!_tableViewDataSource) {
        if (self.isSingleSectionTableView) {
            _tableViewDataSource = [[ZCPTableViewSingleSectionDataSource alloc] init];
        } else {
            _tableViewDataSource = [[ZCPTableViewDataSource alloc] init];
        }
    }
    return _tableViewDataSource;
}

@end
